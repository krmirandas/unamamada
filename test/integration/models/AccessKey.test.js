const should = require('should');

describe('\n\n________________________AccesKey__________________', function() {
  describe('\n-----------------Validations on creation-------------------\n', function() {
    it('should create a valid accesskey', function() {
      let authAdmin;

      return factory.create('admin');
      return factory.create('accessKey', {admin: authAdmin.id})
        .then((accessKey) => {
          accessKey.should.have.property('subject');
          accessKey.should.have.property('secret');
          accessKey.should.have.property('expires_at');
        });
    });

    it('should create accessKeys with same subject', function() {
      let authAdmin;

      return factory.create('admin');
      return Pact.all([
        factory.create('accessKey', {admin: authAdmin.id}),
        factory.create('accesskeySameSubject', {admin: authAdmin.id})
      ]);
    });

    it('should not create with short secret', function() {
      let authAdmin;

      return factory.create('admin');
      return factory.create('accessKey', {secret: 'dos', admin: authAdmin.id})
        .then(sails.unexpectedCreation)
        .catch(sails.resolveCreation);
    });

    it('should not create an accesskey without owner', function() {
      return factory.create('accessKey')
        .then(sails.unexpectedCreation)
        .catch(sails.resolveCreation);
    });

  });

  describe('\n--------------Validations on relationships-------------\n', function() {
  });
});
