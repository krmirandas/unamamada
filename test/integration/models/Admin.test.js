const should = require('should');

describe('\n\n______________________ AdminModel ______________________', function() {
  describe('\n---------------------- Validations on creation ----------------------\n', function() {
    it('should create a valid admin', function() {
      let adminCreated;

      return factory.create('admin')
        .then(function(admin) {
          adminCreated = admin;
          admin.should.be.ok;
          admin.should.have.property('email');
          admin.should.have.property('phone');
          admin.should.have.property('role');
          admin.should.have.property('password_recovery_at');
          admin.should.have.property('password');
          admin.should.have.property('name');
          admin.should.have.property('last_name');
        });
    });
    it('should hash password', function() {
      return factory.create('admin', {password: 'password'})
        .then(function(admin) {
          admin.should.be.ok;
          admin.password.should.not.eql('password');
        });
    });
    it('should not rehash password', function() {
      let password;

      return factory.create('admin')
        .then(function(admin) {
          admin.should.be.ok;
          password = admin.password;
          admin.name = 'Foo';
          return admin.save();
        })
        .then((admin) => {
          return admin.reload();
        })
        .then((admin) => {
          admin.password.should.eql(password);
        });
    });
    it('should not create admin with bad email', function() {
      return factory.create('adminWithBadEmail')
        .then(sails.unexpectedCreation)
        .catch(sails.resolveCreation);
    });
    it('should not create admin with invalid name', function() {
      return factory.create('adminNameInvalid')
        .then(sails.unexpectedCreation)
        .catch(sails.resolveCreation);
    });
    it('should not create admin without name', function() {
      return factory.create('adminWithoutName')
        .then(sails.unexpectedCreation)
        .catch(sails.resolveCreation);
    });
    it('should not create admin without email', function() {
      return factory.create('adminWithoutEmail')
        .then(sails.unexpectedCreation)
        .catch(sails.resolveCreation);
    });
    it('should not create admin with bad phone', function() {
      return factory.create('adminWithBadPhone')
        .then(sails.unexpectedCreation)
        .catch(sails.resolveCreation);
    });
  });

  describe('\n---------------------- Validations on methods ----------------------\n', function() {
    describe('\n- - - - - - - - - - -  removeAllAccessKeys - - - - - - - - - - - \n', function() {
      it('should remove all accesskeys', function() {
        let admin;

        return factory.create('admin')
          .then((adminCreated) => {
            admin = adminCreated;
            return factory.create('accessKey', {admin: admin.id});
          })
          .then((accessKeyFound) => {
            should.exist(accessKeyFound);
          });
      });
    });
  });
  describe('\n---------------------- Validations on functions ----------------------\n', function() {

  });
});
