const should = require('should');

describe('\n\n________________________AuthManager__________________', function() {

  describe('\n- - - - - - - - - - -  removeAllAccessKeys - - - - - - - - - - - \n', function() {
    it('should remove all accesskeys', function() {
      let admin;

      return factory.create('admin')
        .then((adminCreated) => {
          admin = adminCreated;
          return factory.create('accessKey', {admin: admin.id});
        })
        .then((accessKeyFound) => {
          should.exist(accessKeyFound);
        });

    });
  });
});
