const request = require('supertest');
const should = require('should');
const sinon = require('sinon');
let adminJWT;

describe('\n\n______________________ AdminController ______________________', function() {
  beforeEach(function() {
    sails.ohmypolicies.fakepolicy = function(){};
    adminJWT = authtest.generateJWTTestForAdmin();
  });

  afterEach(function () {
    delete sails.ohmypolicies.fakepolicy;
  });

  /**********************************************
   **           GET  /admins                    **
   ***********************************************/
  describe('\n---------------------- get ----------------------\n', function() {
    it('should return all admins', function() {


      return factory.create('admin')
        .then(() => {
          return request(sails.hooks.http.app).get('/admins')
            .set('Authorization', 'JWT ' + adminJWT)
            .expect(200);
        })
        .then((response) => {
          response.body.should.have.property('admins');
          const admins = response.body.admins;

          admins.should.have.length(2); //authAdmin
          admins.forEach((admin) => {
            admin.should.be.ok;
            admin.should.have.property('id');
            admin.should.have.property('name');
            admin.should.have.property('email');
            admin.should.have.property('last_name');
            admin.should.have.property('phone');
            admin.should.have.property('role');
          });
          const adminFormated = admins.find((admin) => {
            return admin.id == authAdmin.id;
          });
        });
    });
    it('should return error with admin role', function() {
      return factory.create('adminManager')
        .then((adminManager) => {
          return factory.create('accessKey', {
            subject: adminManager.email,
            admin: adminManager.id
          });
        })
        .then((accessKey) => {
          return request(sails.hooks.http.app).get('/admins')
            .set('Authorization', 'JWT ' + authtest.generateJWTWithValues('admin', accessKey))
            .expect(403)
            .then((response) => {
              response.body.should.have.property('code', 4031);
            });
        })
    });
    it('should return error unauthorized ', function() {
      return factory.create('adminManager')
        .then((adminManager) => {
          return factory.create('accessKey', {
            subject: adminManager.email,
            admin: adminManager.id
          });
        })
        .then((accessKey) => {
          return request(sails.hooks.http.app).get('/admins')
            .set('Authorization', '---y---')
            .expect(401)
            .then((response) => {
              response.body.should.have.property('code', 4014);
            });
        });
    });
    it('error JWT iat is too big', function() {
      const dateInThePast = Date.now() + 60 * 60 * 60 * 60 * 1000;
      const clock = sinon.useFakeTimers(dateInThePast);
      let token = '';

      return factory.create('adminManager')
        .then((adminManager) => {
          return factory.create('accessKeyiat', {
            subject: adminManager.email,
            expires_at: new Date(),
            admin: adminManager.id
          });
        })
        .then((accessKey) => {
          token += authtest.generateJWTWithValues('admin', accessKey);
          clock.restore();
          return request(sails.hooks.http.app).get('/admins')
            .set('Authorization', ' JWT ' + token)
            .expect(401)
            .then((response) => {
              response.body.should.have.property('code', 4013);
            });
        });
    });
    it('error: JWT expired iat', function() {
      const dateInThePast = Date.now() - 60 * 60 * 60 * 60 * 1000;
      const clock = sinon.useFakeTimers(dateInThePast);
      let token = '';

      return factory.create('adminManager')
        .then((adminManager) => {
          return factory.create('accessKeyiat', {
            subject: adminManager.email,
            expires_at: new Date(),
            admin: adminManager.id
          });
        })
        .then((accessKey) => {
          token += authtest.generateJWTWithValues('admin', accessKey);
          clock.restore();
          return request(sails.hooks.http.app).get('/admins')
            .set('Authorization', ' JWT ' + token)
            .expect(401)
            .then((response) => {
              response.body.should.have.property('code', 4013);
            });
        });
    });
    it('No iat found in JWT', function() {
      const dateInThePast = Date.now() - 60 * 60 * 60 * 60 * 1000;
      const clock = sinon.useFakeTimers(dateInThePast);
      let token = '';

      return factory.create('adminManager')
        .then((adminManager) => {
          return factory.create('accessKeyiat', {
            subject: adminManager.email,
            expires_at: new Date(),
            admin: adminManager.id
          });
        })
        .then((accessKey) => {
          token += authtest.generateJWTWithValues('admin', accessKey);
          clock.restore();
          return request(sails.hooks.http.app).get('/admins')
            .set('Authorization', ' JWT ' + token)
            .expect(401)
            .then((response) => {
              response.body.should.have.property('code', 4013);
            });
        });
    });
    it('should return error with admin role', function() {
      const stub1 = sinon.stub(sails.ohmypolicies, 'fakepolicy').value((req) => {
        return new Promise((resolve) => {
          req.authorization = {};

          return resolve();
        });
      });
      const stub = sinon.stub(sails.config.ohmypolicies.admin, 'get').value(['jwtAuth', 'sessionAccess', 'adminAccess', 'fakePolicy', 'adminOwnerAccess']);

      return factory.create('adminManager')
        .then((adminManager) => {

          return factory.create('accessKey', {
            subject: adminManager.email,
            admin: adminManager.id
          });
        })
        .then((accessKey) => {

          return request(sails.hooks.http.app).get('/admins')
            .set('Authorization', 'JWT ' + authtest.generateJWTWithValues('admin', accessKey))
            .expect(500)
            .then((response) => {
              response.body.should.have.property('code', 500);
            });
        })
        .finally(() => {
          stub.restore();
          stub1.restore();
        })
    });
  });


  /**********************************************
   **           POST  /admins                   **
   ***********************************************/
  describe('\n---------------------- create ----------------------\n', function() {
    it('should create an admin owner', function() {
      let admin;
      const email = sails.config.testmail;

      return factory.create('admin')
        .then((adminCreated) => {
          admin = adminCreated;
          return request(sails.hooks.http.app).post('/admins')
            .set('Authorization', 'JWT ' + adminJWT)
            .send({
              email: email,
              name: 'Aleberto',
              last_name: 'Vazquéz Sánchez',
              phone: '+521 5531514009',
              role: 'owner'

            })
            .expect(201);
        })
        .then((response) => {
          response.body.should.have.property('email', email);
          response.body.should.have.property('name', 'Aleberto');
          response.body.should.have.property('last_name', 'Vazquéz Sánchez');
          response.body.should.have.property('phone', '+521 5531514009');
          response.body.should.have.property('role', 'owner');
          return Admin.find({
            where: {
              email: email
            },
            include: [AccessKey],
            rejectOnEmpty: true
          });
        })
        .then((adminFound) => {

          adminFound.should.have.property('email', email);
          adminFound.should.have.property('name', 'Aleberto');
          adminFound.should.have.property('last_name', 'Vazquéz Sánchez');
          adminFound.should.have.property('phone', '+521 5531514009');
          adminFound.should.have.property('role', 'owner');
          should.not.exist(adminFound.password);
          should.exist(adminFound.password_recovery_at);
        });
    });
    it('should return error for invalid role', function() {
      let admin;

      return factory.create('admin')
        .then(() => {
          return request(sails.hooks.http.app).post('/admins')
            .set('Authorization', 'JWT ' + adminJWT)
            .send({
              email: 'alyavasa@abc.com',
              name: 'Aleberto',
              last_name: 'Vazquéz Sánchez',
              phone: '+521 5531514009',
              role: 'MAL'

            })
            .expect(422);
        })
        .then((response) => {
          response.body.should.have.property('code', 422);
          response.body.should.have.property('errors');
          response.body.errors[0].should.have.property('field', 'role');
          response.body.errors[0].should.have.property('code', 42210);
          return Admin.find({
            where: {
              email: 'alyavasa@abc.com'
            }
          });
        })
        .then((adminFound) => {
          should.not.exist(adminFound);
        });
    });
    it('should return error for name with numbers', function() {
      let admin;

      return factory.create('admin')
        .then((adminCreated) => {
          admin = adminCreated;
          return request(sails.hooks.http.app)
            .post('/admins')
            .set('Authorization', 'JWT ' + adminJWT)
            .send({
              email: 'alyavasa@abc.com',
              name: 'Aleberto 2',
              last_name: 'Vazquéz Sánchez',
              phone: '+521 5531514009',
              role: 'owner'
            })
            .expect(422);
        })
        .then((response) => {
          response.body.should.have.property('code', 422);
          response.body.should.have.property('errors');
          response.body.errors[0].should.have.property('field', 'name');
          response.body.errors[0].should.have.property('code', 4227);
          return Admin.find({
            where: {
              email: 'alyavasa@abc.com'
            }
          });
        })
        .then((adminFound) => {
          should.not.exist(adminFound);
        });
    });
  });

  /**********************************************
   **           PATCH  /admins                  **
   ***********************************************/
  describe('\n---------------------- update ----------------------\n', function() {
    it('should update other admin', function() {
      let adminToUpdate, receivedResponse;
      const email = 'alyavasa@email.com';

      return factory.create('admin')
        .then((admin) => {
          adminToUpdate = admin;
          return request(sails.hooks.http.app).patch('/admins/' + adminToUpdate.id)
            .set('Authorization', 'JWT ' + adminJWT)
            .send({
              email: email, //Should ignore field
              name: 'Aleberto',
              last_name: 'Vazquéz Sánchez',
              phone: '+521 5531514009',
              role: 'owner'
            }).expect(200);
        })
        .then((response) => {
          response.body.should.have.property('id', adminToUpdate.id);
          response.body.email.should.not.eql(email);
          response.body.should.have.property('name', 'Aleberto');
          response.body.should.have.property('last_name', 'Vazquéz Sánchez');
          response.body.should.have.property('phone', '+521 5531514009');
          response.body.should.have.property('role', 'owner');
          return Admin.find({
            where: {
              id: adminToUpdate.id
            },
            rejectOnEmpty: true
          });
        })
        .then((adminFound) => {
          adminFound.should.have.property('name', 'Aleberto');
          adminFound.should.have.property('last_name', 'Vazquéz Sánchez');
          adminFound.should.have.property('phone', '+521 5531514009');
          adminFound.should.have.property('role', 'owner');
          should.not.exist(adminFound.password_recovery_at);
        });
    });
  });

  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   * *            AUTHORIZATION                                     * *
   * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */


  /**********************************************
   **           POST  /admin/login               **
   ***********************************************/
  describe('\n---------------------- login ----------------------\n', function() {
    it('should login an admin with valid credentials and logout', function() {
      let secret;

      return request(sails.hooks.http.app).post('/admins/session')
        .send({
          email: 'authseed@email.com',
          password: 'password'
        })
        .expect(200)
        .then((response) => {
          const body = response.body;

          body.should.have.property('admin');
          body.admin.should.have.property('email', 'authseed@email.com');
          body.should.have.property('access_key');
          body.access_key.should.have.property('subject', 'authseed@email.com');
          body.access_key.should.have.property('secret');
          secret = body.access_key.secret;
          return Admin.find({
            where: {
              email: body.admin.email
            },
            include: {
              model: AccessKey
            },
            rejectOnEmpty: true
          })
            .then((admin) => {

              return request(sails.hooks.http.app).delete('/admins/session')
                .set('Authorization', 'JWT ' + authtest.generateJWTWithValues('admin', 'authseed@email.com', secret))
                .expect(204);
            });
        });
    });
    it('should not login admin with wrong email', function() {

      return request(sails.hooks.http.app).post('/admins/session')
        .send({
          email: 'authseedWRONG@email.com',
          password: 'password'
        })
        .expect(401)
        .then((response) => {
          const body = response.body;

          body.should.have.property('code', 4012);
        });
    });
    it('should not login admin with wrong password', function() {

      return request(sails.hooks.http.app).post('/admins/session')
        .send({
          email: 'authseed@email.com',
          password: 'passwordWRONG'
        })
        .expect(401)
        .then((response) => {
          const body = response.body;

          body.should.have.property('code', 4012);
        });
    });
  });

  /***********************************************
   **            DELETE  /admin/session          **
   ***********************************************/

  /** This tests, test jwtAuth Policie **/
  describe('\n-----------------logout-----------------\n', function() {
    it('should logout admin', function() {
      return request(sails.hooks.http.app).delete('/admins/session')
        .set('Authorization', 'JWT ' + adminJWT)
        .expect(204)
        .then(() => {
          return AccessKey.findAll({
            where: {
              subject: 'authseed@email.com'
            }
          });
        })
        .then((accesskeys) => {
          return request(sails.hooks.http.app)
            .delete('/admins/session')
            .set('Authorization', 'JWT ' + adminJWT)
            .expect(401);
        });
    });
    it('should return unauth for jwt malformed sometihnWeird', function() {
      return request(sails.hooks.http.app)
        .delete('/admins/session')
        .set('Authorization', 'JWT ' + 'somethingWeird')
        .expect(401)
        .then((response) => {
          const body = response.body;

          body.should.have.property('code', 4011);
        });
    });
    it('should return unauth for no jwt', function() {
      const stub1 = sinon.stub(sails.ohmypolicies, 'fakepolicy').value((req) => {
        return new Promise((resolve) => {
          req.authorization = {};

          return resolve();
        });
      });
      const stub = sinon.stub(sails.config.ohmypolicies.admin, 'logout').value(['jwtAuth', 'sessionAccess', 'fakePolicy', 'adminAccess']);


      return request(sails.hooks.http.app)
        .delete('/admins/session')
        .set('Authorization', 'JWT ' + adminJWT)
        .expect(403)
        .then((response) => {
          const body = response.body;

          body.should.have.property('code', 4031);
        })
        .finally(() => {
          stub.restore();
          stub1.restore();
        })
    });
    it('should no logout for unauth', function() {
      const stub1 = sinon.stub(sails.ohmypolicies, 'fakepolicy').value((req) => {
        return new Promise((resolve) => {
          req.authorization = {};


          return resolve();
        });
      });
      const stub = sinon.stub(sails.config.ohmypolicies.admin, 'logout').value(['jwtAuth', 'fakePolicy', 'sessionAccess', 'adminAccess', 'adminOwnerAccess']);


      return request(sails.hooks.http.app)
        .delete('/admins/session')
        .set('Authorization', 'JWT ' + adminJWT)
        .expect(401)
        .then((response) => {
          const body = response.body;

          body.should.have.property('code', 4011);
        })
        .finally(() => {
          stub.restore();
          stub1.restore();
        })
    });
    it('should return unauth for header with wrong type', function() {
      return request(sails.hooks.http.app).delete('/admins/session')
        .set('Authorization', 'Basic ' + 'somethingWeird')
        .expect(401)
        .then((response) => {
          const body = response.body;

          body.should.have.property('code', 4014);
        });
    });
    it('should return unauth for header missing', function() {
      return request(sails.hooks.http.app).delete('/admins/session')
        .expect(401)
        .then((response) => {
          const body = response.body;

          body.should.have.property('code', 4014);
        });
    });
    it('should return unauth for jwt with not found subject', function() {

      return request(sails.hooks.http.app).delete('/admins/session')
        .set('Authorization', 'JWT ' + authtest.generateJWTWithValues('admin', 'notFOUND@email.com', authAdmin.AccessKey.secret))
        .expect(401);
    });
    it('should return unauth for jwt with wrong secret', function() {
      return request(sails.hooks.http.app).delete('/admins/session')
        .set('Authorization', 'JWT ' + authtest.generateJWTWithValues('admin', authAdmin.email, 'WRONG'))
        .expect(401)
        .then((response) => {
          const body = response.body;

          body.should.have.property('code', 4011);
        });
    });
    it('should return unauth for NO AccessKey in request', function() {
      return request(sails.hooks.http.app).delete('/admins/session')
        .set('Authorization', 'JWT ' + authtest.generateJWTWithValues('admin', 'WRONG', 'WRONG', 'WRONG'))
        .expect(401)
        .then((response) => {
          const body = response.body;

          body.should.have.property('code', 4011);
        });
    });
  });

  /***********************************************
   **            POST  /admin/password           **
   ***********************************************/

  describe('\n-----------------requestPasswordReset-----------------\n', function() {
    it('should request a password change', function() {
      let admin;
      let oldAccessKey;

      return factory.create('admin', {
        email: sails.config.testmail,
        password_recovery_at: Date.now()
      })
        .then((adminCreated) => {
          admin = adminCreated;

          return factory.create('accessKey', {
            admin: admin.id,
            subject: admin.email
          });
        })
        .then((oldAccessKeyForRecovery) => {
          oldAccessKey = oldAccessKeyForRecovery;
          return request(sails.hooks.http.app).post('/admins/password')
            .send({
              email: admin.email
            })
            .expect(200);
        })
        .then(() => {
          return Admin.findById(admin.id, {
            include: AccessKey
          });
        })
        .then((adminPopulated) => {
          should.exist(adminPopulated);
          should.exist(adminPopulated.password_recovery_at);
          adminPopulated.should.have.property('AccessKey');
          adminPopulated.AccessKey.id.should.not.eql(oldAccessKey.id);
        });
    });
    it('should return 404 for not found email', function() {
      let admin;
      let oldAccessKey;

      return factory.create('admin', {
        email: sails.config.testmail,
        password_recovery_at: Date.now()
      })
        .then((adminCreated) => {
          admin = adminCreated;
          return factory.create('accessKey', {
            admin: admin.id,
            subject: admin.email
          });
        })
        .then((oldAccessKeyForRecovery) => {
          oldAccessKey = oldAccessKeyForRecovery;
          return request(sails.hooks.http.app).post('/admins/password')
            .send({
              email: 'emailNotInDB@email.com'
            })
            .expect(404)
        });
    });
  });


  /***********************************************
   **            PUT  /admin/change_password     **
   ***********************************************/

  describe('\n-----------------changePassword-----------------\n', function() {
    it('should change admin password with sesion', function() {
      const oldPassword = authAdmin.password;

      return request(sails.hooks.http.app).put('/admins/change_password')
        .set('Authorization', 'JWT ' + adminJWT)
        .send({
          password: 'newPassword',
          password_confirmation: 'newPassword'
        })
        .expect(204);
    });
    it('should respond error for missmath passwords', function() {
      const oldPassword = authAdmin.password;

      return request(sails.hooks.http.app).put('/admins/change_password')
        .set('Authorization', 'JWT ' + adminJWT)
        .send({
          password: 'newPassword',
          password_confirmation: 'newPasswordBad'
        })
        .expect(422)
        .then((response) => {
          response.body.should.have.property('code', 422);
          response.body.should.have.property('errors');
          response.body.errors.should.have.length(1);
          response.body.errors[0].should.have.property('code', 4225);
        });
    });
  });

  /***********************************************
   **            PUT  /admin/password           **
   ***********************************************/

  describe('\n-----------------resetPassword-----------------\n', function() {
    it('should reset admin password', function() {
      let admin;
      let oldPassword;

      return factory.create('admin', {
        email: sails.config.testmail,
        password_recovery_at: moment()
      })
        .then((adminCreated) => {
          admin = adminCreated;
          oldPassword = admin.password;
          return factory.create('accessKey', {
            admin: admin.id,
            subject: admin.email
          });
        })
        .then((accessKey) => {
          return request(sails.hooks.http.app).put('/admins/password')
            .set('Authorization', 'JWT ' + authtest.generateJWTWithValues('admin', accessKey))
            .send({
              password: 'newPassword',
              password_confirmation: 'newPassword'
            })
            .expect(204);
        })
        .then(() => {
          return Admin.findById(admin.id, {
            include: [AccessKey]
          });
        })
        .then((adminPopulated) => {
          should.exist(adminPopulated);
          should.not.exist(adminPopulated.password_recovery_at);
        });
    });
    it('should return unauth for normal accesskey', function() {

      return request(sails.hooks.http.app).put('/admins/password')
        .set('Authorization', 'JWT ' + adminJWT)
        .send({
          password: 'newPassword',
          password_confirmation: 'newPassword'
        })
        .expect(403)
        .then((response) => {
          response.body.should.have.property('code', 4032);
        });

    });
    it('should return unauth for normal accesskey', function() {
      const stub1 = sinon.stub(sails.ohmypolicies, 'fakepolicy').value((req) => {
        return new Promise((resolve) => {
          req.language = {};

          return resolve();
        });
      });
      const stub = sinon.stub(sails.config.ohmypolicies.admin, 'resetpassword').value(['fakePolicy', 'language', 'jwtAuth', 'recoveryAccess', 'adminAccess']);
      let admin;
      let oldPassword;

      return request(sails.hooks.http.app).put('/admins/password')
        .set('Authorization', 'JWT ' + adminJWT)
        .send({
          password: 'newPassword',
          password_confirmation: 'newPassword'
        })
        .expect(403)
        .then((response) => {
          response.body.should.have.property('code', 4032);
        })
        .finally(() => {
          stub.restore();
          stub1.restore();
        })
    });
    it('should return unauth for normal accesskey', function() {
      const stub1 = sinon.stub(sails.ohmypolicies, 'fakepolicy').value((req) => {
        return new Promise((resolve) => {
          req.authorization.accessKey = null;

          return resolve();
        });
      });
      const stub = sinon.stub(sails.config.ohmypolicies.admin, 'resetpassword').value(['language', 'jwtAuth', 'fakePolicy', 'recoveryAccess', 'adminAccess']);
      let admin;
      let oldPassword;

      return request(sails.hooks.http.app).put('/admins/password')
        .set('Authorization', 'JWT ' + adminJWT)
        .send({
          password: 'newPassword',
          password_confirmation: 'newPassword'
        })
        .expect(401)
        .then((response) => {
          response.body.should.have.property('code', 4011);
        })
        .finally(() => {
          stub.restore();
          stub1.restore();
        })
    });
    it('should return forbidden for no recovery set', function() {
      return factory.create('admin')
        .then((adminCreated) => {
          admin = adminCreated;
          oldPassword = admin.password;
          return factory.create('accessKey', {
            admin: admin.id,
            subject: admin.email
          });
        })
        .then((accessKey) => {

          return request(sails.hooks.http.app).put('/admins/password')
            .set('Authorization', 'JWT ' + authtest.generateJWTWithValues('admin', accessKey))
            .send({
              password: 'newPass',
              password_confirmation: 'newPass'
            })
            .expect(403);
        })
        .then((response) => {
          response.body.should.have.property('code', 4032);
        });
    });

    it('should return error for not log enough password', function() {
      return factory.create('admin', {
        password_recovery_at: moment()
      })
        .then((adminCreated) => {
          admin = adminCreated;
          oldPassword = admin.password;
          return factory.create('accessKey', {
            admin: admin.id,
            subject: admin.email
          });
        })
        .then((accessKey) => {
          return request(sails.hooks.http.app).put('/admins/password')
            .set('Authorization', 'JWT ' + authtest.generateJWTWithValues('admin', accessKey))
            .send({
              password: 'short',
              password_confirmation: 'short'
            })
            .expect(400);
        })
        .then((response) => {

          response.body.should.have.property('code', 400);
          response.body.should.have.property('errors');
          response.body.errors.should.have.length(2);
        });
    });

    it('should return error for too long password', function() {
      return factory.create('admin', {
        password_recovery_at: moment()
      })
        .then((adminCreated) => {
          admin = adminCreated;
          oldPassword = admin.password;
          return factory.create('accessKey', {
            admin: admin.id,
            subject: admin.email
          });
        })
        .then((accessKey) => {
          return request(sails.hooks.http.app).put('/admins/password')
            .set('Authorization', 'JWT ' + authtest.generateJWTWithValues('admin', accessKey))
            .send({
              password: 'toooooooLongPassword',
              password_confirmation: 'toooooooLongPassword'
            })
            .expect(400);
        })
        .then((response) => {
          response.body.should.have.property('code', 400);
          response.body.should.have.property('errors');
          response.body.errors.should.have.length(2);
        });
    });
    it('should return error for password missmatch', function() {
      return factory.create('admin', {
        password_recovery_at: moment()
      })
        .then((adminCreated) => {
          admin = adminCreated;
          oldPassword = admin.password;
          return factory.create('accessKey', {
            admin: admin.id,
            subject: admin.email
          });
        })
        .then((accessKey) => {
          return request(sails.hooks.http.app).put('/admins/password')
            .set('Authorization', 'JWT ' + authtest.generateJWTWithValues('admin', accessKey))
            .send({
              password: 'newPassword',
              password_confirmation: 'newPasswordBad'
            })
            .expect(422);
        })
        .then((response) => {
          response.body.should.have.property('code', 422);
          response.body.should.have.property('errors');
          response.body.errors.should.have.length(1);
          response.body.errors[0].should.have.property('code', 4225);
        });
    });
  });

});
