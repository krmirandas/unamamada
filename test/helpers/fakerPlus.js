'use strict';
const rand = require('rand-token');
const slug = require('slug');
const randomstring = require('randomstring');
const moment = require('moment');

module.exports = {

  secret: () => {
    return rand.generate(20);
  },

  bannerTitle: () => {
    return 'Visit ' + faker.address.country();
  },

  slug: () => {
    return slug(faker.random.word(1).substr(0, 10).toLowerCase(), '');
  },

  integerArray: () => {
    return [faker.random.number()];
  },

  domain: () => {
    return 'https://google.com';
  },

  overAgeBirthday: () => {
    const date = moment(faker.date.past()).subtract(18, 'years').format();

    return date;
  },

  credit_card: () => {
    const numbersStart = randomstring.generate({
      length: 6,
      charset: 'numeric'
    });
    const numbersEnd = randomstring.generate({
      length: 4,
      charset: 'numeric'
    });

    return numbersStart + '******' + numbersEnd;
  },

  yid: () => {
    const fakerYid = 50000 + faker.random.number({
      min: 0,
      max: 9999
    });

    return fakerYid;
  },

  //Wraper for randomstring
  string: (options) => {
    return function() {
      const string = randomstring.generate(options);

      return string;
    };
  },

  serviceName: () => {
    const fakerName = faker.commerce.productName();

    return fakerName.slice(0, 15);
  },

  fullPackage: () => {
    let packageCreation;
    return Promise.all([
        factory.create('city'),
        factory.create('city'),
        factory.create('city'),
        factory.create('spot'),
        factory.create('spot'),
      ])
      .then(([g1, g2, g3, s1, s2]) => {
        return factory.create('package', {
            minCapacity: 5,
            maxCapacity: 10,
            minDaysReserve: 10,
            maxDaysReserve: 100,
            category: 1001,
          })
          .tap((pack) => {
            return ModelUtils.createInfoModels(pack, _.pick({
              es: {
                name: 'Paquete',
                description: 'El paquete prueba'
              },
              en: {
                name: 'Pack 1',
                description: 'Test Package'
              }
            }, ['en', 'es']));
          })
          .then((pack) => {
            packageCreation = pack;

            return Gallery.bulkCreate([{
                type: 'package',
                city: g1.id,
                has_image: true
              },
              {
                type: 'package',
                city: g2.id,
                has_image: true
              },
              {
                type: 'package',
                city: g3.id,
                has_image: true
              }
            ], {
              returning: true
            })
          })
          .tap((galleries) => {
            return packageCreation.update({
              featured: galleries[0].id
            });
          })
          .then((galleries) => {
            return packageCreation.addGalleries(galleries.map(i => i.id).slice(1));
          })
          .then(() => {
            return packageCreation.addCities([g1, g2, g3]);
          })
          .then(() => {
            return Promise.all([
                factory.create('comment',{
                  isArrival: true,
                  comment: 'Frente al Walmart',
                  package: packageCreation.id,
                  language: 4002,
                  spot: s1.id
                }),
                factory.create('comment',{
                  isArrival: true,
                  comment: 'In front of Walmart',
                  package: packageCreation.id,
                  language: 4001,
                  spot: s1.id
                }),
                factory.create('comment',{
                  isArrival: false,
                  comment: 'Frente al Walmart',
                  package: packageCreation.id,
                  language: 4002,
                  spot: s2.id
                }),
                factory.create('comment',{
                  isArrival: false,
                  comment: 'In front of Walmart',
                  package: packageCreation.id,
                  language: 4001,
                  spot: s2.id
                }),

              ])
          })
          .then(() => {
            const recurrence = {};

            recurrence.recurrence = 127;
            recurrence.validTo = moment().startOf('day').add(10, 'd');
            recurrence.validFrom = moment().startOf('day').add(30, 'd');
            recurrence.package = packageCreation.id;
            return Recurrence.create(recurrence);
          })
          .then((dias) => {
            return packageCreation.setLogos([1, 2]);
          })
          .then(() => {
            return factory.create('itinerary', {
              package: packageCreation.id
            });
          })
          .then((itinerary) => {
            return Promise.all([
              factory.create('itineraryinfo', {
                itinerary: itinerary.id
              }),
              factory.create('itineraryinfo', {
                language: 4002,
                itinerary: itinerary.id
              })
            ])
          })
          .then(() => {
            return Package.scope({
                method: ['withAllInfo']
              })
              .findById(packageCreation.id, {
                rejectOnEmpty: sails.hooks.errorhandler.rejectOnEmptyModel('Package')
              });
          })
          .then((packageWithInfo) => {
            return packageWithInfo.allInfo();
          });
      });
  },

  fullTailormade: () => {
    let packageCreation;

    return Promise.all([
        factory.create('city'),
        factory.create('city'),
        factory.create('city'),
      ])
      .then(([g1, g2, g3]) => {

        return factory.create('tailormade', {category: 2})
          .tap((tailormade) => {
            return ModelUtils.createInfoModels(tailormade, _.pick({
              es: {
                name: 'Paquete',
                description: 'El paquete prueba'
              },
              en: {
                name: 'Pack 1',
                description: 'Test Package'
              }
            }, ['en', 'es']));
          })
          .then((tailormade) => {
            packageCreation = tailormade;
            return Gallery.bulkCreate([{
                type: 'package',
                city: g1.id,
                has_image: true
              },
              {
                type: 'package',
                city: g2.id,
                has_image: true
              },
              {
                type: 'package',
                city: g3.id,
                has_image: true
              }
            ], {
              returning: true
            })
          })
          .tap((galleries) => {
            return packageCreation.update({
              featured: galleries[0].id
            });
          })
          .then((galleries) => {
            return packageCreation.addGalleries(galleries.map(i => i.id).slice(1));
          })
          .then(() => {
            return packageCreation.addCities([g1, g2, g3]);
          })
          .then(() => {
            return Tailormade.scope({
                method: ['withAllInfo']
              })
              .findById(packageCreation.id, {
                rejectOnEmpty: sails.hooks.errorhandler.rejectOnEmptyModel('Tailormade')
              });
          })
          .then((packageWithInfo) => {
            return packageWithInfo.allInfo();
          });
      })
  }
};
