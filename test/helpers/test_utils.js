'use strict';

module.exports = {

  addAtributesTo: function(baseObject, attributesObject) {
    const newObject = {};

    _.keys(baseObject).concat(_.keys(attributesObject)).forEach(function(key) {
      newObject[key] = attributesObject[key] !== undefined ? attributesObject[key] : baseObject[key];
    });

    return newObject;
  },

  /**
  * Used in tests instead of busboy used in http as middleware
  */
  bufferToFileBusboy: function(buffer, mimetype, exededSize) {
    return {
      data: buffer,
      name: name,
      mimetype: type.mime,
      size: buffer.length,
      encoding: '7bit'
    };
  }

};
