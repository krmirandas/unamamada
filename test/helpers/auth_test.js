'use strict';

module.exports = {

  /*** GET AUTH INSTANCES ***/
  getAuthAdmin: function() {
    return Admin.findById(4001, {include: [AccessKey]});
  },

  /*** GENERATE AUTH  ***/
  generateJWTTestForAdmin: function() {
    return sails.services.jwtutils.generateFromValues({
      subject: 'authseed@email.com',
      secret: 'GyK2UwEUgAsLYqH25Lw2',
      type: 'admin'
    });
  },
  generateJWTWithValues: function(type, subject, secret) {
    const passingAccessKey = typeof subject == 'object';
    let values;

    if (passingAccessKey) {
      values = subject;
      values.type = type;
    } else {
      values = {
        subject: subject,
        secret: secret,
        type: type
      };
    }
    return sails.services.jwtutils.generateFromValues(values);
  },

  generateJWTWithExpirediat: function(type, subject, secret) {
    const passingAccessKey = typeof subject == 'object';
    let values;

    if (passingAccessKey) {
      values = subject;
      values.type = type;
    } else {
      values = {
        subject: subject,
        secret: secret,
        type: type
      };
    }
    const jwt = sails.services.jwtutils.generateFromValues(values);

    return jwt;
  }


};
