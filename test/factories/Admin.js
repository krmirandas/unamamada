module.exports = function(factory) {

  factory.define('admin', sails.models.admin)
    .attr('email', faker.internet.email)
    .attr('password', faker.internet.password)
    .attr('phone', '+52 5531340063')
    .attr('name', faker.name.firstName)
    .attr('role', 'manager')
    .attr('last_name', faker.name.lastName);

  //-----------ValidInstances
  factory.define('adminOwner', sails.models.admin)
    .attr('email', faker.internet.email)
    .attr('password', faker.internet.password)
    .attr('phone', '+52 5531340063')
    .attr('name', faker.name.firstName)
    .attr('role', 'owner')
    .attr('last_name', faker.name.lastName);

  factory.define('adminManager', sails.models.admin)
    .attr('email', faker.internet.email)
    .attr('password', faker.internet.password)
    .attr('phone', '+52 5531340063')
    .attr('name', faker.name.firstName)
    .attr('role', 'manager')
    .attr('last_name', faker.name.lastName);

  factory.define('adminWithoutPassword', sails.models.admin)
    .attr('email', faker.internet.email)
    .attr('phone', '+52 5531340063')
    .attr('role', 'owner')
    .attr('name', faker.name.firstName)
    .attr('last_name', faker.name.lastName);

  // -----------InvalidInstances
  factory.define('adminNameInvalid', sails.models.admin)
    .parent('admin')
    .attr('name', 'Name invalid 222');

  factory.define('adminWithBadEmail', sails.models.admin)
    .parent('admin')
    .attr('email', 'adminWithBadEmail');

  factory.define('adminWithoutName', sails.models.admin)
    .attr('email', faker.internet.email)
    .attr('password', faker.internet.password)
    .attr('phone', '+52 5531340063')
    .attr('last_name', faker.name.lastName);

  factory.define('adminWithoutEmail', sails.models.admin)
    .attr('password', faker.internet.password)
    .attr('phone', '+52 5531340063')
    .attr('name', faker.name.firstName)
    .attr('last_name', faker.name.lastName);

  factory.define('adminWithBadPhone', sails.models.admin)
    .parent('admin')
    .attr('phone', '55 31 340063');
};
