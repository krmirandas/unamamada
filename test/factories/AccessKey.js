
module.exports = function(factory) {
  // Valid instance
  factory.define('accessKey', sails.models.accesskey)
    .attr('subject', faker.internet.email)
    .attr('secret', fakerPlus.secret);

  factory.define('accessKeyiat', sails.models.accesskey)
    .attr('subject', faker.internet.email)
    .attr('secret', fakerPlus.secret)
    .attr('expires_at', fakerPlus.secret);

  factory.define('accesskeySameSubject', sails.models.accesskey)
    .parent('accessKey');

  factory.define('accessKeyWithoutSubject', sails.models.accesskey)
    .attr('secret', fakerPlus.secret);
};
