/**
 * Internal Errors
 *
 * An HTTP Response with specific format
 *
 * Each response has format:
 *
 * error {
 * 	code: '[H'TTP_CODE]-[INTERNAL_CODE],
 * 	message: "Response message",
 * 	detailedInfo: [See https://docs.google.com/document/d/1YXdZ4MqyrtBVpcaBbbsEmhO1LW-N6oMF_H66NnEalNk/edit]
 * }
 */
module.exports.errorhandler = {
  codes: {

    'badRequest': 400,
    'missing': 4001,
    'maformedJson': 4002,
    'malformedList': 4003,
    'diplicates': 4004,
    'customerOrGuestNeeded': 4008,
    'type_is_video': 4009,
    'galleryPackage': 4011,
    'query_needed': 4112,

    'unauthorized': 401,
    'invalidToken': 4011,
    'invalidCredentials': 4012,
    'expiredToken': 4013,
    'authHeaderMissing': 4014,

    'forbidden': 403,
    'withoutPermitions': 4031,
    'noReset': 4032,
    'invalidSignature': 4033,

    'notFound': 404,
    'routeNotFound': 4041,
    'notFound {{model}}': 4042,
    'notFoundOPOS': 4043,
    'notFoundSale': 4044,
    'saleNotFromClient': 4041,

    'conflict': 409,
    'currencyExist': 4091,
    'emailUnique': 4092,
    'saleCompleted': 4093,
    'associatedToCustomer': 4094,
    'conflictOrder': 4095,
    'saleClosed': 4096,
    'takenSeat': 4097,
    'blockedSeat': 4098,
    'used_gallery': 4099,
    'duplicate_city': 40910,
    'duplicate_code': 40911,
    'CurrencyDefault': 4100,
    'bad_code_amount': 40912,
    'invalid_date': 40913,
    'duplicate_promo': 40914,
    'bad_promo_percentage': 40915,
    'invalid_code': 40916,

    'gone': 410,
    'resourceGone': 4101,
    'saleExpired': 4102,
    'noPlaces': 4103,

    'preconditionFailed': 412,
    'invalidTotalSale': 4121,
    'saleAlreadyAttended': 4122,
    'invalidTotals': 4123,
    'runCancelled': 4124,
    'seatsNotSelected': 4125,
    'runNotAvailable': 4126,
    'city_in_use': 4127,
    'city_with_images': 4128,
    'inactive_package': 4129,
    'active_package_needed': 41210,

    'unsupportedMediaType': 415,
    'unsupportedExtension {{fileName}} {{acceptedMimes}}': 4151,
    'unsupportedSizeSmall {{fileName}} {{expectedH}} {{expectedW}}': 4152,
    'unsupportedProportion {{fileName}} {{expectedH}} {{expectedW}}': 4153,
    'unsupportedIsnotSquare': 4154,
    'unsupportedSizeBig {{fileName}}': 4154,

    'invalid': 422,
    'notNull': 4221,
    'lenHave {{value}}': 4222,
    'lenMost {{value}}': 4223,
    'invalidFormat': 4224,
    'passwordMissmatch': 4225,
    'invalid_coordinates': 4226,

    'lenExact {{value}}': 4226,
    'alphabet': 4227,
    'alphanumeric': 4228,
    'invalidPhone': 4229,
    'in {{value}}': 42210,
    'isType {{value}}': 42210,
    'invalidBoolean': 42211,
    'invalidLink': 42213,
    'invalidAge': 42214,
    'invalidDate {{format}}': 42215,
    'invalidDateRange': 42216,
    'MissingFaqs': 4005,
    'RepeatedPositions': 4006,
    'max %s': 42211,
    'min %s': 42211,
    'noExchangeValue': 42212,
    'isMustNotTailormade': 42213,
    'recurrence_or_days': 42215,
    '{{min}} {{max}} capacity': 42216,
    'duplicateImages': 42217,
    'missing_arguments': 42218,
    'datesNoMatch': 42219,
    'noImages': 42220,
    '{{min}} {{max}} length_number': 42221,
    'invalidPosition': 42222,
    'min {{value}}': 42223,


    'locked': 423,
    'changeOwnRole': 4231,
    'changeOwnActive': 4232,

    'failedDependency': 424,
    'emailError': 4241,
    'cloudError': 4242,
    'serviceUnavailable': 4243,

    'serverError': 500,
    'databaseError': 5001,
    'serverErrorCostume {{message}}': 5002,
    'communicationError': 5003


  }
};
