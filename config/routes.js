/**
 * Route Mappings
 *
 * Your routes map URLs to views and controllers.
 * NotFound response in case there is no match
 */
module.exports.routes = {

  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   * *                          ADMIN                               * *
   * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
  //---------------------------------------------------
  // ADMIN CONTROLLER
  //---------------------------------------------------
  /*** ADMIN CONTROLLER ***/
  'get /admins': {
    controller: 'Admin',
    action: 'get'
  },
  'post /admins/session': {
    controller: 'Admin',
    action: 'login',
    body: 'login'
  },
  'delete /admins/session': {
    controller: 'Admin',
    action: 'logout'
  },
  'put /admins/password': {
    controller: 'Admin',
    action: 'resetPassword',
    body: 'changePasswordValidation'
  },
  'put /admins/change_password': {
    controller: 'Admin',
    action: 'changePassword',
    body: 'changePasswordValidation'
  },
  'post /admins/password': {
    controller: 'Admin',
    action: 'requestPasswordReset',
    body: 'requestPasswordReseValidation'
  },
  'post /admins': {
    controller: 'Admin',
    action: 'create',
    body: 'createAdmin'
  },
  'patch /admins/:id': {
    controller: 'Admin',
    action: 'update',
    body: 'updateAdmin'
  },

  'get /status': function(req, res) {
    return res.ok();
  }

};
