/**
 * Policies
 *
 * Defines the policies for every method in the controller
 */
const basePolicies = ['idParamsValidator', 'request'];
const adminPolicies = basePolicies.concat(['jwtAuth', 'sessionAccess', 'adminAccess']);

module.exports.ohmypolicies = {
  admin: {
    'get': adminPolicies.concat(['adminOwnerAccess']),
    'create': adminPolicies.concat(['adminOwnerAccess']),
    'update': adminPolicies.concat(['adminOwnerAccess']),
    /* Authorization */
    'logout': adminPolicies,
    'resetpassword': ['jwtAuth', 'request', 'recoveryAccess'],
    'changepassword': ['jwtAuth', 'request', 'sessionAccess'],
    'login': basePolicies
  }

};
