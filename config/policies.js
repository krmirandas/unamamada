/**
 * Policies
 *
 * Defines the policies for every method in the controller, all redirected
 * to ohmypolicy
 */

module.exports.policies = {
  '*': 'ohmypolicy'
};
