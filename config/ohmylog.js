/**
 * Custom logger
 */

module.exports.ohmylog = {
  file: {
    filename: 'unearth-travel.log',
    level: 'info',
    maxDays: 10
  },

  console: {
    level: 'info'
  },

  exceptionFilename: 'exceptions.log',

  defaultLabel: 'UNEARTH',

  rotate: true,

  length: 32

};
