module.exports.ohmybody = {


  //---------------------------------------------------
  //Auth
  //---------------------------------------------------
  createAdmin: {
    required: {
      email: 'string',
      name: 'string->3-100',
      last_name: 'string->3-100',
      phone: 'string->3-100',
      role: 'string->3-15'
    }
  },
  updateAdmin: {
    optional: {
      name: 'string->3-100',
      last_name: 'string->3-100',
      phone: 'string->3-100',
      role: 'string->3-15'
    }
  },

  login: {
    required: {
      email: 'string->3-100',
      password: 'string->6-18'
    }
  },

  requestPasswordReseValidation: {
    required: {
      email: 'string->3-100'
    }
  },

  resetPasswordValidation: {
    required: {
      password: 'string->6-16',
      password_confirmation: 'string->6-16'
    }
  },

  changePasswordValidation: {
    required: {
      password: 'string->6-16',
      password_confirmation: 'string->6-16'
    }
  }
};
