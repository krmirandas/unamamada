/**
 * HTTP Server Settings
 * (sails.config.http)
 *
 * Configuration for the underlying HTTP server in Sails.
 * Only applies to HTTP requests (not WebSockets)
 *
 * For more information on configuration, check out:
 * http://sailsjs.org/#!/documentation/reference/sails.config/sails.config.http.html
 */
const moment = require('moment');
const colors = require('colors');
const morgan = require('morgan');
const busboy = require('busboy-body-parser');
const crypto = require('crypto');
const multer = require('multer');
const lazy = require('lazy.js');
const helmet = require('helmet');

var colorStatus = function(status) {
  if (status < 300) {
    return colors.green(status);
  } else if (status < 400) {
    return colors.blue(status);
  } else if (status < 500) {
    return colors.yellow(status);
  }
  return colors.red(status);

};
var colorTime = function(time) {

  if (time < 100) {
    return colors.green(time + ' ms');
  } else if (time < 500) {
    return colors.blue(time + ' ms');
  } else if (time < 1000) {
    return colors.yellow(time + ' ms');
  }
  return colors.red(time + ' ms');

};

module.exports.http = {

  /****************************************************************************
   *                                                                           *
   * Express middleware to use for every Sails request. To add custom          *
   * middleware to the mix, add a function to the middleware config object and *
   * add its key to the "order" array. The $custom key is reserved for         *
   * backwards-compatibility with Sails v0.9.x apps that use the               *
   * `customMiddleware` config option.                                         *
   *                                                                           *
   ****************************************************************************/

  middleware: {

    /***************************************************************************
     *                                                                          *
     * The order in which middleware should be run for HTTP request. (the Sails *
     * router is invoked by the "router" middleware below.)                     *
     *                                                                          *
     ***************************************************************************/
    order: [
      'startRequestTimer',
      // 'cookieParser',
      'helmet',
      'fileParser',
      'bodyParser',
      'myRequestLogger',
      'morganLogger',
      // 'session',
      //'handleBodyParserError',
      'compress',
      //'methodOverride',
      //'poweredBy',
      '$custom',
      //'helmet',
      'router',
      // 'www',
      // 'favicon',
      '404',
      '500'
    ],


    /****************************************************************************
     *                                                                           *
     *                             LOGGER                                        *
     *                                                                           *
     ****************************************************************************/

    myRequestLogger: function(req, res, next) {
      if (req.method === 'OPTIONS') {
        return next();
      }
      const log = sails.hooks.ohmylog.log('config:http');
      const id = crypto.randomBytes(6).toString('hex').slice(0, 5);

      req.reqId = id;
      log.info('-----------------------------------------------------------------------');
      log.info('[' + req.reqId + ']   ' + (req.method + ' ' + req.path));
      log.info('-----------------------------------------------------------------------');
      log.silly('Arguments in body:', req.body);
      log.silly('Arguments in query:', req.query);
      return next();
    },
    helmet: helmet({
      noCache: true
    }),
    morganLogger: morgan(function(tokens, req, res) {
      if (req.method === 'OPTIONS') {
        return null;
      }
      return [
        '[' + req.reqId + ']  ',
        moment().format('MM/D/YY, HH:mm'),
        '············ ',
        tokens.method(req, res),
        req.path,
        colorStatus(tokens.status(req, res)),
        '-',
        colorTime(tokens['response-time'](req, res))
      ].join(' ');
    }, {
      stream: {
        write: function(profile) {
          // sails.hooks.ohmylog.log('config:http').info(profile);
        }
      }
    }),

    /***************************************************************************
     *                                                                          *
     * The body parser that will handle incoming multipart HTTP requests. By    *
     * default as of v0.10, Sails uses                                          *
     * [skipper](http://github.com/balderdashy/skipper). See                    *
     * http://www.senchalabs.org/connect/multipart.html for other options.      *
     *                                                                          *
     * Note that Sails uses an internal instance of Skipper by default; to      *
     * override it and specify more options, make sure to "npm install skipper" *
     * in your project first.  You can also specify a different body parser or  *
     * a custom function with req, res and next parameters (just like any other *
     * middleware function).                                                    *
     *                                                                          *
     ***************************************************************************/
    bodyParser: (function _configureBodyParser() {
      var skipper = require('skipper');
      var opts = {
        limit: 10000000,
        parameterLimit: 10000,
        strict: true
      };
      var middlewareFn = skipper(opts);

      return middlewareFn;
    })()
  }

  /***************************************************************************
   *                                                                          *
   * The number of seconds to cache flat files on disk being served by        *
   * Express static middleware (by default, these files are in `.tmp/public`) *
   *                                                                          *
   * The HTTP static cache is only active in a 'production' environment,      *
   * since that's the only time Express will cache flat-files.                *
   *                                                                          *
   ***************************************************************************/

  // cache: 31557600000
};
