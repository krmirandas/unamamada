/**
 * Deploy front environment settings
 *
 * Settings for production envirnment
 *
 */

module.exports = {

  /***************************************************************************
   * Database                                                                *
   ***************************************************************************/
  connection: 'db_staging_connection',

  models: {
    migrate: 'safe',
    connection: 'db_staging_connection'
  },

  /***************************************************************************
   * Port                                                                    *
   ***************************************************************************/
  port: 1441,

  email: {
    testMode: false
  },

  adminBasePath: 'https://staging.admin.unearth.jaque.dev/',
  
};
