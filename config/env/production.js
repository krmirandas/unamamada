/**
 * Deploy front environment settings
 *
 * Settings for production envirnment
 *
 */

module.exports = {

  /***************************************************************************
   * Database                                                                *
   ***************************************************************************/
  connection: 'db_production_connection',

  models: {
    migrate: 'safe',
    connection: 'db_production_connection'
  }

};
