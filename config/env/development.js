/**
 * Deploy front environment settings
 *
 * Settings for development envirnment
 *
 */

module.exports = {

  /***************************************************************************
   * Database                                                                *
   ***************************************************************************/
  connection: 'db_development_connection',

  models: {
    migrate: 'safe',
    connection: 'db_development_connection'
  },

  /***************************************************************************
   * Port                                                                    *
   ***************************************************************************/
  port: 1337,

  adminBasePath: 'https://development.admin.unearth.jaque.dev/',

  testmail: 'kevin.miranda@test.com',

};
