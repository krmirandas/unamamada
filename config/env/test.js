/**
 * Deploy front environment settings
 *
 * Settings for test envirnment wile runing insude tests
 *
 */

module.exports = {


  /***************************************************************************
   * Database                                                                *
   ***************************************************************************/
  connection: 'db_test_connection',

  models: {
    connection: 'db_test_connection',
    migrate: 'drop'
  },

  /***************************************************************************
   * Port                                                                    *
   ***************************************************************************/
  port: 1442,

  paypal: {

    return_url: 'https://development.web.unearth.jaque.dev',
    cancel_url: 'https://development.web.unearth.jaque.dev'

  },

  testmail: 'kevin.miranda@test.com',


};
