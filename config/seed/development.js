module.exports.seed = {
  development: [
    /*************  ADMIN   *************/
    {
      model: 'admin',
      data: {
        id: 5001,
        email: 'krvinmiranda29@ciencias.unam.mx',
        password: 'password',
        phone: '+52 5531340063',
        name: 'Kevin',
        role: 'owner',
        last_name: 'Gomez'
      }
    }
  ]

};
