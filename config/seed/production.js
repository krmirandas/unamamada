module.exports.seed = {
  production: [
    /*************  ADMIN   *************/
    {
      model: 'admin',
      data: {
        id: 5001,
        email: 'sofiagomez@unearth-travels.com',
        password: 'password',
        phone: '+52 5531340063',
        name: 'Sofía',
        role: 'owner',
        last_name: 'Gomez'
      }
    },
  ]


};
