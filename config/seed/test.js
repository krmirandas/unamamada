module.exports.seed = {
  test: [

    /*************  ADMIN   *************/
    {
      model: 'admin',
      def: 'authAdmin',
      data: {
        id: 4001,
        email: 'authseed@email.com',
        password: 'password',
        phone: '+52 016641047400',
        name: 'AdminNameSeed',
        role: 'owner',
        last_name: 'AdminLastNameSeed'
      }
    },
    /*************  ACCESSKEY   *************/
    {
      model: 'accesskey',
      data: {
        id: 4001,
        subject: 'authseed@email.com',
        secret: 'GyK2UwEUgAsLYqH25Lw2',
        admin: 4001
      }
    }
  ]

};
