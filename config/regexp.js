module.exports.regexp = {
  letters: {
    args: /^[A-Za-zÁÉÍÓÚÜÑáéíóúüñ][A-Za-zÁÉÍÓÚÜÑáéíóúüñ\- ]*$/,
    msg: 'alphabet'
  },

  digits: {
    args: /^\d*$/,
    msg: 'invalidFormat'
  },

  alphanumeric: {
    args: /^[A-Za-z\d]*$/,
    msg: 'alphanumeric'
  },

  phone: {
    args: /^\(?\+?[0-9]{1,3}\)? ?-?[0-9]{1,3} ?-?[0-9]{3,5} ?-?[0-9]{4}$/,
    msg: 'invalidPhone'
  },

  currency: {
    args: /^([A-Z])*$/,
    msg: 'invalidFormat'
  },

  device_uuid: {
    args: /^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/,
    msg: 'invalidFormat'
  },

  url: {
    args: /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w.-]+)+[\w\-._~:/?#[\]@!$&'()*+,;=.]+$/,
    msg: 'invalidFormat'
  },

  social_url: {
    args: /^(https?:\/\/)?([\da-z.-]+).([a-z.]{2,6})([/\w .-]*)*\/?$/,
    msg: 'invalidFormat'
  },

  youtube_url: {
    args: /^(?:https?:\/\/)?(?:m\.|www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/,
    msg: 'invalidVideoUrl'
  },

  email: {
    args: /(([^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*)|(".+"))@(([^<>()[\].,;:\s@"]+.)+[^<>()[\].,;:\s@"]{2,})$/,
    msg: 'invalidFormat'
  },
  hours: {
    args: /^(2[0-3]|[01]?[0-9]):([0-5]?[0-9])$/,
    msg: 'invalidFormat'
  },
  point: {
    args: /^\d+(\.\d{1,2})?$/,
    msg: 'invalidFormat'
  }
};
