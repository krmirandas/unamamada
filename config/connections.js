/**
 * Connections
 * (sails.config.connections)
 *
 * `Connections` are like "saved settings" for your adapters.  What's the difference between
 * a connection and an adapter, you might ask?  An adapter (e.g. `sails-mysql`) is generic--
 * it needs some additional information to work (e.g. your database host, password, user, etc.)
 * A `connection` is that additional information.
 *
 * Each model must have a `connection` property (a string) which is references the name of one
 * of these connections.  If it doesn't, the default `connection` configured in `config/models.js`
 * will be applied.  Of course, a connection can (and usually is) shared by multiple models.
 * .
 * Note: If you're using version control, you should put your passwords/api keys
 * in `config/local.js`, environment variables, or use another strategy.
 * (this is to prevent you inadvertently sensitive credentials up to your repository.)
 *
 * For more information on configuration, check out:
 * http://sailsjs.org/#!/documentation/reference/sails.config/sails.config.connections.html
 */

module.exports.connections = {

  /***************************************************************************
  *                                                                          *
  * Local disk storage for DEVELOPMENT ONLY                                  *
  *                                                                          *
  * Installed by default.                                                    *
  *                                                                          *
  ***************************************************************************/
  // localDiskDb: {
  //   adapter: 'sails-disk'
  // },


  /*************************************************ºKMiranda
  **************************
  *                                                                          *
  * Storage using Vagrant and postgres                                       *
  *                                                                          *
  ***************************************************************************/

  db_development_connection: {
    user: process.env.POSTGRES_USER || 'kevinmiranda',
    port: 5432,
    database: process.env.POSTGRES_DB || 'unearthdb_devel',
    password: process.env.POSTGRES_PASSWORD || '1234',
    dialect: 'postgres',
    options: {
      dialect: 'postgres',
      host: process.env.POSTGRES_HOST || 'localhost',
      port: 5432
    }
  },

  db_test_connection: {
    user: process.env.POSTGRES_USER || 'kevinmiranda',
    port: 5432,
    database: process.env.POSTGRES_DB || 'unearthDB-test',
    password: process.env.POSTGRES_PASSWORD || '1234',
    dialect: 'postgres',
    options: {
      dialect: 'postgres',
      host: process.env.POSTGRES_HOST || 'localhost',
      port: 5432
    }
  },

  db_production_connection: {
    user: process.env.POSTGRES_USER || 'postgres',
    port: 5432,
    database: process.env.POSTGRES_DB || 'unearthDB-devel',
    password: process.env.POSTGRES_PASSWORD || '',
    dialect: 'postgres',
    options: {
      dialect: 'postgres',
      host: process.env.POSTGRES_HOST || 'localhost',
      port: 5432
    }
  },

  db_staging_connection: {
    user: process.env.POSTGRES_USER || 'kevinmiranda',
    port: 5432,
    database: process.env.POSTGRES_DB || 'unearthDB-staging',
    password: process.env.POSTGRES_PASSWORD || '1234',
    dialect: 'postgres',
    options: {
      dialect: 'postgres',
      host: process.env.POSTGRES_HOST || 'localhost',
      port: 5432
    }
  }


};
