module.exports.email = {
  transport: {
    host: 'smtp.mailgun.org',
    port: 465,
    auth: {
      user: 'unearth.travel@mailgun.grupojaque.com',
      pass: '9b0946132065c7e9dbbee05d9524e38e-7bce17e5-d105d41c'
    }
  },

  templates: {
    'resetPasswordAdmin': {
      view: 'resetPasswordAdmin',
      subject: 'Unearth - Restablece tu contraseña'
    },
    'welcomeAdmin': {
      view: 'welcomeAdmin',
      subject: 'Unearth - Establece tu contraseña'
    },
    'newPasswordAdmin': {
      view: 'newPasswordAdmin',
      subject: 'Unearth - Se modificó tu contraseña'
    },
    'comment': {
      view: 'comments',
      subject: 'Comentario en Unearth Travel'
    },
    'reservation': {
      view: 'reservation',
      subject: 'Reservación'
    },
    'reservationClient': {
      view: 'reservationClient',
      subject: 'Reservación'
    },
    'newTailormade': {
      view: 'newTailormade',
      subject: 'Solicitud de experiencia a la medida'
    },
    'capacity': {
      view: 'capacity',
      subject: 'Cupo mínimo no alcanzado para paquete'
    }
  },

  from: 'unearth.travel@mailgun.grupojaque.com',

  url: {

    loginAdmin: function() {
      return sails.config.adminBasePath + '/login';
    },
    setPasswordAdmin: function(secret) {
      return sails.config.adminBasePath + '/change_password?secret=' + secret;
    },
    requestRecoveryAdmin: function() {
      return sails.config.adminBasePath + '/login?recover_password=true';
    }
  }
  //testmail: 'kevin.miranda@example.com', //Email used in tests

  //managerEmail: 'kevin.miranda@grupojaque.com'
};
