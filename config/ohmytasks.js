const pkg = require('../package.json');
const moment = require('moment-timezone');

module.exports.ohmytasks = {
  onLift: function() {
    sails.hooks.ohmylog.info('     '.magenta);
    const dateString = '   ' + new Date();

    sails.hooks.ohmylog.info(dateString);
    sails.hooks.ohmylog.info('   Time        : ' + moment().tz('America/Mexico_City').format('MM/D/YY, HH:mm'));
    sails.hooks.ohmylog.info('   Environment : ' + sails.config.environment);
    sails.hooks.ohmylog.info('   Port        : ' + String(sails.config.port));
    sails.hooks.ohmylog.info('   Log Ohmylog : ' + String(sails.config.ohmylog.console.level));
    sails.hooks.ohmylog.info('   Models      : ' + String(sails.config.models.migrate));
    sails.hooks.ohmylog.info('   Version     : ' + String(pkg.version));
    sails.hooks.ohmylog.info('   Prefix      : ' + String(sails.config.environment || '').yellow);
    sails.hooks.ohmylog.info('  .........................................     '.magenta);
    sails.hooks.ohmylog.info('  ......................................... '.magenta);
  },

  before: function(sails, cb) {
    sails.hooks.ohmylog.info('---------------- Loading config ----------------');
    return cb();
  },

  after: function(sails, cb) {
    sails.hooks.ohmylog.info('--------------------- Done ---------------------\n');
    return cb();
  },

  dirname: 'ohmytasks',
  toDo: [
    {
      tasks: ['seed', 'services'],
      //tasks: ['seed'],
      order: true
    }
  ]
};
