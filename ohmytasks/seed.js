const sequelize_fixtures = require('sequelize-fixtures');

module.exports = function(sails, next) {
  const log = sails.hooks.ohmylog;

  if (sails.config.models.migrate != 'drop' || sails.config.intask || sails.config.SKIP_SEED) {
    return next(null, sails);
  }

  log.info('Starting seed for dropped DB');
  return sequelize_fixtures.loadFixtures(sails.config.seed[sails.config.environment], sails.models).asCallback(
    function(err) {
      if (err) {
        log.error('Error during seed for ' + sails.config.environment);
      } else {
        log.info('  ✓  Seed completed'.green);
      }
      return next(err, sails);
    }
  );
};
