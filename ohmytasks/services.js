/**
 * OhmyTask::Services
 *
 * Task to load `ohmypolicies` and make
 * the policies available.
 */
const include = require('include-all');
const map = require('lodash/map');
const extend = require('lodash/extend');
const bindAll = require('lodash/bindAll');
const path = require('path').resolve;

module.exports = function(sails, next) {
  const policies = {};
  const modules = include({
    dirname: path(sails.config.appPath, 'api/ohmypolicies'),
    filter: /(.+)\.js$/
  });

  map(modules, function(module, name) {
    // Add a reference to the Sails app that loaded the module
    module.sails = sails;
    // Bind all methods to the module context
    bindAll(module);
    // Make them globals
    global[name] = module;
    policies[name.toLowerCase()] = module;
  });
  // Add to sails.policies
  sails.ohmypolicies = {};
  extend(sails.ohmypolicies, policies);
  sails.hooks.ohmylog.info('✓ Ohmypolicies loaded');

  return next(null, sails);
};
