# UNAMamada

## Contenido

* [Documentacion](#documentacion)

* [Requerimientos](#requerimientos)

* [Ejecutar el proyecto](#ejecutar-proyecto)

* [Pruebas](#pruebas)

* [Instalacion](#instalacion)



## Documentacion


Verifica el diagrama entidad relacion [diagrama entidad realción]()

## Requerimientos


* [NodeJS](https://nodejs.org)

	- Instalar utilizando nvm (Node Version Manager)
	- Instalar NodeJS 10.15.3
  - Dentro de la carpeta del proyecto ejecutar `$ nvm use` para cambiarse automaticamente a esa version


* [SailsJS](http://sailsjs.org/get-started)

	- Instalar la version 0.12.13.
    Verifica la version de Sails en ``package.json``  `./node_modules/sails/bin/sails.js --version`

## Ejecutar proyecto

1. Clonar repositorio

2. Instalar modulos de node `$ npm install`

3. Dentro de la carpeta del proyecto ejecuta en terminal `$ sails lift` esto levantara el proyecto en ambiete de desarrollo.

## Pruebas

### Internas
Instalar mocha global
`$ npm install mocha -g`

Ejecutar todas las pruebas:
`$ npm run test:all`

Para ejecutar una prueba particular:
`$FILE=[Nombre] npm test`

## Instalacion

1. Clonar respositorio

2. Moverse a la carpeta del proyecto.

3. Instalar dependencias requeridas

4. Instalar submodulos con el comando `$ npm install`


