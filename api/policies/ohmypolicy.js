/**
* Using ohmypolicies.
*/
const Promise = require('bluebird');
const log = sails.hooks.ohmylog.log('policie:ohmypolicies');
var loadPolicies = function(req, policies) {
  return new Promise((resolve) => {
    const options = req.options;
    const hasPolicyDefinedForController = policies[options.controller] != undefined;

    if (!hasPolicyDefinedForController) {
      return resolve({and: []});
    }

    const policiesForAction = policies[options.controller][options.action];
    const all = policies[options.controller]['*'];
    const hasPolicyDefinedForAction = policiesForAction != undefined || all != undefined;


    if (!hasPolicyDefinedForAction) {
      return resolve({and: []});
    }

    return resolve(policiesForAction || all);
  });
};
/**
*
* policyName: is object or string indicating policy name
*/
var resolvePolicy = function(policyName, req) {

  if (typeof policyName == 'string') {
    return sails.ohmypolicies[policyName.toLowerCase()](req);
  }
  if (policyName.and) {
    return Promise.mapSeries(policyName.and, function(name) {
      return resolvePolicy(name, req);
    });
  } else if (policyName.or) {
    let hasPassed = false;

    return Promise.mapSeries(policyName.or, function(name) {
      if (hasPassed) {
        return null;
      }
      return resolvePolicy(name, req)
        .then(() => {
          hasPassed = true;
          return null;
        })
        .catch((err) => {
          return err;
        });
    })
      .then((errors) => {
        if (hasPassed) {
          return null;
        }
        const finalErrors = _.remove(errors, null);

        if (finalErrors.length == 0) {
          return null;
        }
        throw finalErrors.pop();

      });
  }
  log.error('Policy malformed: ' + policyName);
  throw sails.hooks.errorhandler.create('serverError', 'serverError');


};

module.exports = function(req, res, next) {
  loadPolicies(req, sails.config.ohmypolicies)
    .then((policies) => {
      let newPolicies = policies;

      if (Array.isArray(policies)) {
        newPolicies = {and: policies};
      }
      return resolvePolicy(newPolicies, req);
    })
    .then(() => {
      log.debug('All policies passed');
      return next();
    })
    .catch(res.negotiate);
};
