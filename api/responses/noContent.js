/**
 * 204 (noContent) Handler
 */
const log = sails.hooks.ohmylog.log('responses:noContent');

module.exports = function noContent() {
  const req = this.req;
  const res = this.res;

  res.status(204);
  log.info(req.method + ' ' + req.path + ' responded with');
  return res.json();
};
