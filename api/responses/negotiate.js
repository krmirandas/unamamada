/**
 * 200 (Ok) Handler
 */
const log = sails.hooks.ohmylog.log('responses:negotiate');
const Promise = require('bluebird');

module.exports = function negotiate(error) {
  const res = this.res;
  const req = this.req;

  new Promise((resolve) => {
    if (error.name == 'ErrorHandler') {
      return resolve(error);
    }
    return resolve(sails.hooks.errorhandler.fromSequelize(error));
  })
    .catch((err) => {
      log.error('UNEXPECTED ERROR', err);
      return sails.hooks.errorhandler.create('serverError', 'serverError');
    })
    .then((err) => {

      const errorBody = err.respond(req.__);
      const status = err.status();

      log.error('Responding ' + status);
      log.error(errorBody);
      res.status(status);
      return res.json();
    });
};
