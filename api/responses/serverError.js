/**
 * 500 (Server Error) Response
 * ServerError
 * Code: 500-00
 * When an unexpectd server error ocurees or database error raised.
 */

const log = sails.hooks.ohmylog.log('responses:serverError');

module.exports = function serverError(result) {
  const res = this.res;

  log.error('UNEXPECTED ERROR. WITHOUT NEGOTIATE. REDIRECTED', result);
  log.error(result);
  const error = sails.hooks.errorhandler.create('serverError', 'serverError');

  return res.negotiate(error);

};
