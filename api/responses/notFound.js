/**
 * 404 (Not Found) Handler
 * Not found
 * When the route couldnt be found or the model wiht given id.
 * Detailed info will be the route or the query with one the model wasn't found
 */

module.exports = function notFound() {
  const res = this.res;
  const error = sails.hooks.errorhandler.create('notFound', 'routeNotFound');

  return res.negotiate(error);
};
