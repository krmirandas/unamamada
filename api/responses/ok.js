/**
 * 200 (Ok) Handler
 */
const log = sails.hooks.ohmylog.log('responses:ok');

module.exports = function ok(response) {
  const req = this.req;
  const res = this.res;

  res.status(200);
  const fullResponse = response || {};

  log.info(req.method + ' ' + req.path + ' responded with', fullResponse);
  return res.json(fullResponse);
};
