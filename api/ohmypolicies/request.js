/**
 * Request arguments validations
 *
 * @module      :: Policy
 * @description :: Validates body of request and sets it with clean values
 */
const Promise = require('bluebird');
const log = sails.hooks.ohmylog.log('policies:request');

module.exports = function(req) {

  return Promise.resolve()
    .then(() => {
      if (!req.options.body && !req.options.query) {
        log.silly('Skipping body & query validation');
        return;
      }
      const validations = typeof req.options.body == 'object' ? req.options.body : sails.config.ohmybody[req.options.body];

      if (validations) {
        return RequestHandler.validateRequiredArguments(validations, req.body);
      }
    })
    .then((cleanBody) => {
      req.body = cleanBody;

      if (typeof req.options.query == 'object' && req.options.query.constructor == Array) {
        return RequestHandler.cleanQuery(req.options.query, req.query);
      }
      if (req.options.query) {
        return RequestHandler.validateRequiredArguments(req.options.query, req.query);
      }
      return {};
    })
    .then((cleanQuery) => {
      req.query = cleanQuery;
    });
};
