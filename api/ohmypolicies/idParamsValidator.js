/**
 * idParamsValidator
 *
 * @module      :: Policy
 * @description :: Verifies all params in url with key ending in id are numbers
 * @throws  :: 400 if type of param is not number
 */
var log = sails.hooks.ohmylog.log('policie:idParamsValidator');

module.exports = (req) => {

  return new Promise((resolve) => {
    const paramsKeys = Object.keys(req.params || {});

    paramsKeys.forEach((key) => {
      if (key.slice(-2) == 'id') {
        const isNumber = !isNaN(parseInt(req.params[key]));

        if (!isNumber) {
          log.error('Invalid id in request for param ' + key);
          throw sails.hooks.errorhandler.create('badRequest', 'badRequest');
        }
      }
    });
    return resolve();
  });
};
