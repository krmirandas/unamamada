/**
 * adminAccess
 *
 * @module      :: Policy
 * @description :: Verifies the access was made by an Admin
 * @throws  :: 401 if no accesskey in header
 *                 | not accesskey for recovery
 *             410 AccesskeyExpired
 */

var log = sails.hooks.ohmylog.log('policie:adminAccess');

module.exports = (req) => {

  return new Promise((resolve) => {

    if (!req.authorization.admin) {
      log.error('Request was not made by admin');
      throw sails.hooks.errorhandler.create('forbidden', 'withoutPermitions');
    }

    return resolve();
  });

};
