/**
 * adminOwnerAccess
 *
 * @module      :: Policy
 * @description :: Verifies the admin has owner access
 * @throws  :: 403 if admin is not owner
 */

var log = sails.hooks.ohmylog.log('policie:adminOwnerAccess');

module.exports = (req) => {

  return new Promise((resolve) => {
    if (!req.authorization.admin) {
      log.error('In policie adminOnwnerAuth without admin in request');
      throw sails.hooks.errorhandler.create('serverError', 'serverError');
    }

    const correctRole = req.authorization.admin.role === 'owner';

    if (!correctRole) {
      log.info('User doesn\'t have the correct role');
      throw sails.hooks.errorhandler.create('forbidden', 'withoutPermitions');
    }

    return resolve();
  });

};
