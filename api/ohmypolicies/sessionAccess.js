/**
 * jwtAuth
 *
 * @module      :: Policy
 * @description :: Verifies access key referes to a session.
 * @throws  :: 401 if no header found
 *                 | header didnt match pattern
 *                 | header isnt JWT
 *                 | header undecodable
 *
 */

var log = sails.hooks.ohmylog.log('policie:jwtAuth');

module.exports = (req) => {

  return new Promise((resolve) => {

    if (!req.authorization.accessKey) {
      log.error('No AccessKey in request');
      throw sails.hooks.errorhandler.create('unauthorized', 'invalidToken');
    }


    return resolve();
  });

};
