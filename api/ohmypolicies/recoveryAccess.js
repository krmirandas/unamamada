/**
 * jwtAuth
 *
 * @module      :: Policy
 * @description :: Verifies access key referes to recovery. Must go After jwtAuth
 * @throws  :: 401 if no accesskey in header
 *                 | not accesskey for recovery
 *             403 No reset expected
 *             410 AccesskeyExpired
 */

var log = sails.hooks.ohmylog.log('policie:jwtAuth');

module.exports = (req) => {

  return new Promise((resolve) => {

    if (!req.authorization.accessKey) {
      log.error('No AccessKey in request');
      throw sails.hooks.errorhandler.create('unauthorized', 'invalidToken');
    }
    if (!req.authorization.accessKey.subject) {
      log.error('Not an access key for recovery');
      throw sails.hooks.errorhandler.create('unauthorized', 'invalidToken');
    }
    const user = req.authorization.admin;

    if (!user.password_recovery_at) {
      throw sails.hooks.errorhandler.create('forbidden', 'noReset');
    }

    return resolve();
  });
};
