/**
 * Admin.js
 *
 * @description :: Admin model
 */
// const Op = Sequelize.Op;

module.exports = {
  attributes: {
    email: {
      type: Sequelize.STRING(50),
      allowNull: false,
      unique: true,
      validate: {
        isEmail: sails.hooks.errorhandler.validations.email
      }
    },
    password: {
      type: Sequelize.STRING
    },
    name: {
      type: Sequelize.STRING(20),
      allowNull: false,
      validate: {
        len: sails.hooks.errorhandler.validations.len(3, 20),
        is: sails.config.regexp.letters
      }
    },
    last_name: {
      type: Sequelize.STRING(20),
      allowNull: false,
      validate: {
        len: sails.hooks.errorhandler.validations.len(3, 20),
        is: sails.config.regexp.letters
      }
    },
    phone: {
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
        is: sails.config.regexp.phone
      }
    },
    role: {
      type: Sequelize.ENUM,
      values: ['manager', 'owner'],
      validate: {
        isIn: sails.hooks.errorhandler.validations.in(['manager', 'owner'])
      }
    },
    password_recovery_at: {
      type: Sequelize.DATE,
      get() {
        const recoveryAt = this.getDataValue('password_recovery_at');

        return recoveryAt ? recoveryAt.getTime() : null;
      }
    }
  },
  associations: () => {
    Admin.hasOne(AccessKey, {
      foreignKey: 'admin',
      allowNull: false,
      onDelete: 'cascade'
    });
  },

  options: {
    underscored: true,
    hooks: {
      beforeCreate: AuthManager.hashPassword,
      beforeUpdate: AuthManager.hashPassword
    },

    /********* CLASS METHODS *********/
    classMethods: {
      basicAttributes: function() {
        return ['id', 'name', 'email', 'last_name', 'phone', 'role'];
      }
    },

    /********* INSTANCE METHODS *********/
    instanceMethods: {
      /*** FORMATS ***/
      formatBasic: function() {
        return _.pick(this, Admin.basicAttributes());
      },

      //Most have Pages populated
      format: function() {
        const admin = this;
        const adminFormated = admin.formatBasic();

        return adminFormated;
      }

    }
  }
};
