/**
 * AccessKey.js
 *
 * @description :: Represents the authentication status of an user
 */
// const randToken = require('rand-token');
const log = sails.hooks.ohmylog.log('models:accessKey');
const moment = require('moment');

module.exports = {
  attributes: {
    subject: {
      type: Sequelize.STRING(50),
      allowNull: false,
      validate: {
        isEmail: sails.hooks.errorhandler.validations.email
      }
    },
    secret: {
      type: Sequelize.STRING(20),
      allowNull: false,
      validate: {
        len: sails.hooks.errorhandler.validations.len(20)
      }
    },
    expires_at: {
      type: Sequelize.DATE,
      get() {
        const expiresAt = this.getDataValue('expires_at');

        return expiresAt ? expiresAt.getTime() : null;
      }
    }
  },
  associations: () => {
    AccessKey.belongsTo(Admin, {
      foreignKey: 'admin',
      allowNull: false
    });
  },
  options: {
    underscored: true,
    indexes: [
      {
        unique: true,
        fields: ['subject']
      }
    ],
    hooks: {
      beforeCreate: function(values) {
        const hasOwner = values.admin;

        if (!hasOwner) {
          log.error('Creating access key without owner');
          throw sails.hooks.errorhandler.create('serverError', 'serverError');
        }

        values.expires_at = moment().add(sails.config.timing.loginExpiration, 'hours');

      }
    },
    /********* CLASS METHODS *********/
    classMethods: {
      getFromPayload: function(payload) {
        return new Promise((resolve) => {

          const hasValidType = payload.type && _.includes(['admin'], payload.type);

          if (!hasValidType) {
            log.error('Payload without valid type.');
            log.error(payload);
            throw sails.hooks.errorhandler.create('unauthorized', 'invalidToken');
          }
          const query = {
            where: {
              subject: payload.sub
            },
            rejectOnEmpty: sails.hooks.errorhandler.rejectOnEmpty('unauthorized', 'invalidToken', {})
          };

          if (payload.type == 'admin') {
            query.include = [Admin];
          }
          return resolve(AccessKey.find(query));
        });
      }
    },

    /********* INSTANCE METHODS *********/
    instanceMethods: {

      /*** FORMATS ***/
      formatBasic: function() {
        return _.pick(this, ['subject', 'secret', 'expires_at']);
      },


      hasExpired: function() {
        return this.expires_at < moment().valueOf();
      },

      getUserModelName: function() {
        return 'admin';
      },

      //Must have Admin and Customer Populated
      getUser: function() {
        return this.Admin;
      }

    }
  }
};
