/**
 * AdminController.js
 *
 * @description :: Server-side logic for managing Admins
 */
// const log = sails.hooks.ohmylog.log('controllers:admin');

module.exports = {

  get: function(req, res) {
    return Admin.findAll({})
      .then((admins) => {
        return {
          admins: admins.map((admin) => {
            return admin.format();
          })
        };
      })
      .then(res.ok)
      .catch(res.negotiate);
  },


  create: function(req, res) {
    let admin;

    req.body.password_recovery_at = moment();
    return Admin.create(req.body)
      .then((adminCreated) => {
        admin = adminCreated;
      })
      .then(() => {
        return AuthManager.requestPasswordRecovery(admin);
      })
      .then((recoveryAccessKey) => {
        admin.recoveryAccessKey = recoveryAccessKey.dataValues;
        AuthManager.sendUserTemplate('welcomeAdmin', admin);
      })
      .then(() => {
        return admin.format();
      })
      .then(res.created)
      .catch(res.negotiate);
  },

  update: function(req, res) {
    let adminToUpdate;

    return Admin.find({
      where: {
        id: req.params.id
      },
      rejectOnEmpty: sails.hooks.errorhandler.rejectOnEmptyModel('Admin')
    })
      .then((adminFound) => {
        adminToUpdate = adminFound;
        return adminToUpdate.update(_.pick(req.body, ['name', 'last_name', 'phone', 'role']));
      })
      .then(() => {
        return adminToUpdate.reload();
      })
      .then((adminReloaded) => {
        return adminReloaded.format();
      })
      .then(res.ok)
      .catch(res.negotiate);
  },

  /********* AUTHORIZATION *********/
  login: function(req, res) {
    let admin;


    return AuthManager.validateLoginData(req.body.email, req.body.password, Admin)
      .then((adminFound) => {
        admin = adminFound;
        return AuthManager.doLogin(admin);
      })
      .then(([accessKey]) => {
        return {
          admin: admin.formatBasic(),
          access_key: accessKey.formatBasic()
        };
      })
      .then(res.ok)
      .catch(res.negotiate);
  },

  logout: function(req, res) {
    return req.authorization.accessKey.destroy()
      .then(res.noContent)
      .catch(res.negotiate);
  },

  requestPasswordReset: function(req, res) {

    return AuthManager.validateEmail(req.body.email, Admin)

      .then((adminFound) => {
        return sequelize.transaction(function() {
          return AuthManager.requestPasswordRecovery(adminFound);
        })
          .then((recoveryAccessKey) => {
            adminFound.recoveryAccessKey = recoveryAccessKey.dataValues;
            AuthManager.sendUserTemplate('resetPasswordAdmin', adminFound);
          });
      })
      .then(res.ok)
      .catch(res.negotiate);
  },

  resetPassword: function(req, res) {

    const samePasswords = req.body.password === req.body.password_confirmation;

    if (!samePasswords) {
      throw sails.hooks.errorhandler.create('invalid', {
        password_confirmation: 'passwordMissmatch'
      });
    }
    return sequelize.transaction(function() {
      return AuthManager.changePassword(req.authorization.admin, req.body.password);
    })
      .then(() => {
        AuthManager.sendUserTemplate('newPasswordAdmin', req.authorization.admin);
      })
      .then(res.noContent)
      .catch(res.negotiate);
  },


  changePassword: function(req, res) {

    const samePasswords = req.body.password === req.body.password_confirmation;

    if (!samePasswords) {
      throw sails.hooks.errorhandler.create('invalid', {
        password_confirmation: 'passwordMissmatch'
      });
    }
    return sequelize.transaction(function() {
      return AuthManager.changePassword(req.authorization.admin, req.body.password);
    })
      .then(() => {
        AuthManager.sendUserTemplate('newPasswordAdmin', req.authorization.admin);
      })
      .then(res.noContent)
      .catch(res.negotiate);
  }
};
