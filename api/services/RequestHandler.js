/**
* Usage:
*
* To validate the arguments in the body of a request use the
* function validateArguments(arrayOfRequiredArgumentsInBody,body);
*
*/
const log = sails.hooks.ohmylog.log('services:requestHandler');
const Promise = require('bluebird');
const defaults = require('lodash/defaults');
const has = Object.prototype.hasOwnProperty;

class Validator {
  constructor(values) {
    this.invalid = [];
    this.missing = [];
    this.validations = values;
  }

  validateLength(arg, validation, key) {
    // lalala
    if (!validation) {
      return arg;
    }

    const length = validation.split('-');
    const isNumber = typeof arg == 'number';
    const actual = isNumber ? arg : arg.length;
    const min = parseInt(length[0]);
    const max = parseInt(length[1] || actual + 1);

    if (actual < min || actual > max) {
      this.invalid.push({
        fields: [key],
        message: {
          meta: {
            min: min,
            max: max
          },
          message: '{{min}} {{max}} length_' + (isNumber ? 'number' : 'default')
        }
      });
    }

    return arg;
  }

  validateArgument(arg, validation, key) {
    if (typeof validation == 'string') {
      // Basic validations:
      // string -> min-max
      // number -> min-max
      // array -> min-max
      // object
      // boolean
      const vals = validation.split('->');

      if (vals[0] == 'array' && arg.constructor.name == 'Array') {
        return this.validateLength(arg, vals[1], key);
      }

      if (vals[0] == 'json' && typeof arg == 'object') {
        return arg;
      }

      if (vals[0] == 'regexp') {
        const expression = vals[1].startsWith('/') ?
          vals[1].substr(1, vals[1].length - 2) : vals[1];
        const regexp = new RegExp(expression);

        if (regexp.test(arg)) {
          return arg;
        }

        this.invalid.push({
          fields: [key],
          message: `${vals[0]} regexp_` + (vals[2] || 'default')
        });

        return null;
      }

      if (typeof arg == vals[0]) {
        return this.validateLength(arg, vals[1], key);
      }

      if (typeof arg != vals[0] && typeof arg == 'string') {
        try {
          const parsed = JSON.parse(arg);

          if (typeof parsed != 'string') {
            return this.validateArgument(parsed, validation, key);
          }
        } catch (err) {
          this.invalid.push({
            fields: [key],
            message: `${vals[0]} type_expected`
          });

          return null;
        }
      }

      this.invalid.push({
        fields: [key],
        message: `${vals[0]} type_expected`
      });

      return null;
    }
    // Complex Validations
    // {
    //  type
    //  (Array) items
    //  (Object) validate
    // }
    if (validation.type == 'multiple') {
      const context = this;

      return validation.is.reduce(function(currentValue, currentValidation) {
        const currentClean = context.validateArgument(currentValue, currentValidation, key);

        return currentClean || currentValue;
      }, arg);
    }

    let argParsed = arg;

    if (typeof arg == 'string') {
      try {
        argParsed = JSON.parse(arg);
      } catch (err) {
        this.invalid.push({
          fields: [key],
          message: `${validation.type} type_expected`
        });

        return null;
      }
    }
    if (validation.type == 'array' && argParsed.constructor.name == 'Array') {
      this.validateLength(argParsed, validation.len, key);
      // Array validations
      if (validation.items) {
        argParsed.forEach((value, index) => {
          argParsed[index] = this.validateArgument(value, validation.items, `${key}[${index}]`);
        });
      }

      return argParsed;
    }

    if (validation.type == 'json' && typeof argParsed == 'object') {
      // Object validations
      const clean = this.validateAll(validation.validate, argParsed, key + '.');

      return clean[0];
    }

    this.invalid.push({
      fields: [key],
      message: `${validation.type} type_expected`
    });

    return null;
  }

  validateAll(validations, body, prefixKey = '') {
    const cleanBody = {};

    defaults(validations, {
      required: {},
      optional: {},
      conditional: {}
    });

    // Required arguments
    for (const key in validations.required) {
      if (!has.call(body, key) || body[key] === null) {
        this.missing.push(prefixKey + key);
      } else {
        cleanBody[key] = this.validateArgument(body[key], validations.required[key], prefixKey + key);
      }
    }

    // Optional arguments
    for (const key in validations.optional) {
      if (has.call(body, key) && body[key] !== null) {
        cleanBody[key] = this.validateArgument(body[key], validations.optional[key], prefixKey + key);
      }
    }

    const errors = this.__manageErrors();

    if (errors.length > 0) {
      return [cleanBody, errors];
    }

    // Conditional arguments
    for (const key in validations.conditional) {
      const validation = validations.conditional[key](body);

      if (validation) {
        if (has.call(body, key) && body[key] !== null) {
          cleanBody[key] = this.validateArgument(body[key], validation, prefixKey + key);
        } else {
          this.missing.push(prefixKey + key);
        }
      }
    }
    return [cleanBody, this.__manageErrors()];
  }

  validate(body) {
    if (!this.validations.required && !this.validations.optional && !this.validations.conditional) {
      log.verbose('Validating arguments in body..');
      const cleanBody = this.validateArgument(body, this.validations, 'body');
      const errors = this.__manageErrors();

      return [cleanBody, errors];
    }

    return this.validateAll(this.validations, body);
  }

  __manageErrors() {
    const errors = [];

    if (this.missing.length > 0) {
      errors.push({
        message: 'missing',
        fields: this.missing
      });
    }

    if (this.invalid.length > 0) {

      this.invalid.map((e) => {
        errors.push(e);
      });
    }

    return errors;
  }
}

module.exports = {
  /*** Validations ***/
  validateRequiredArguments: function(validations, body) {
    return new Promise(function(resolve) {
      if (!body) {
        return resolve({});
      }
      const val = new Validator(validations);
      const data = val.validate(body);

      if (data[1].length > 0) {
        throw sails.hooks.errorhandler.create('badRequest', data[1]);
      }
      return resolve(data[0]);
    });


  },

  cleanQuery: function(possibleKeys, query) {
    const clean = {};

    possibleKeys.forEach((key) => {
      const values = key.split('.');

      if (!has.call(query, values[0]) || query[values[0]] == '') {
        return;
      }

      let val = query[values[0]];

      if (!values[1] || values[1] === 'none' || typeof val === values[1]) {
        clean[values[0]] = val;
        return;
      }

      switch (values[1]) {
      case 'int':
        val = Number.parseInt(val);
        if (isNaN(val)) {
          log.silly('Ignoring key ' + values[0] + ' not an int');
          break;
        }
        clean[values[0]] = val;
        break;
      case 'string':
        clean[values[0]] = String(val);
        break;
      case 'boolean':
        switch (val.toLowerCase()) {
        case 'true':
        case '1':
        case 'yes':
          clean[values[0]] = true;
          break;
        case 'false':
        case '0':
        case 'no':
          clean[values[0]] = false;
          break;
        }
        break;
      default:
        log.silly('Removing ' + values[0] + ' as given, invalid type');
        break;
      }
    });

    return clean;
  }
};
