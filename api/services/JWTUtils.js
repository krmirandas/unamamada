/**
* Utils for generation of JWT or validations
*/
const jwt = require('jsonwebtoken');
const log = sails.hooks.ohmylog.log('services:jwt');

module.exports = {
  decodeJWT: function(token) {
    return new Promise((resolve) => {
      const decoded = jwt.decode(token, {complete: true});

      if (decoded === null) {
        log.error('JWT was not decoded ' + token);
        throw sails.hooks.errorhandler.create('unauthorized', 'invalidToken');
      }

      resolve(decoded);
    });
  },

  verifyJWT: function(token, secret) {
    return new Promise((resolve) => {
      jwt.verify(token, secret);
      resolve();
    })
      .catch(function(err) {
        log.error('Couldn\'t verify JWT with secret', err.message);
        throw sails.hooks.errorhandler.create('unauthorized', 'invalidToken');
      });
  },

  extractJWT: function(req) {
    return new Promise((resolve) => {
      const authorization = req.headers.authorization;
      const re = /(\S+)\s+(\S+)/;

      // without header or something weird in it
      if (!authorization || typeof authorization !== 'string') {
        throw sails.hooks.errorhandler.create('unauthorized', 'authHeaderMissing');
      }

      const authParams = authorization.match(re);

      if (!authParams) { // no match for required authorization
        log.error('Authorization header isn\'t valid.');
        log.error(authorization);
        throw sails.hooks.errorhandler.create('unauthorized', 'authHeaderMissing');
      }

      const validAuthType = authParams[1] === 'JWT';

      if (validAuthType) {
        return resolve(authParams[2]);
      }

      log.error('Included authorization header, but has invalid type');
      throw sails.hooks.errorhandler.create('unauthorized', 'authHeaderMissing');
    });
  },


  validateIatJWT: function(payload) {
    return new Promise((resolve) => {
      if (!payload.iat) {
        log.error('No iat found in JWT');
        throw sails.hooks.errorhandler.create('unauthorized', 'invalidToken');
      }
      const now = moment();
      const iatWithWindow = moment(payload.iat * 1000).add(sails.config.timing.iatExpiration, 'seconds');
      const isInTheTimeWindow = iatWithWindow > now;

      log.debug('iat:' + payload.iat);
      log.debug('iatWithWindow:' + iatWithWindow.format());
      log.debug('now:' + now.format() + '   seconds:' + now.valueOf() / 1000);
      if (!isInTheTimeWindow) {
        log.error('JWT iat expired iat:' + payload.iat);
        throw sails.hooks.errorhandler.create('unauthorized', 'expiredToken');
      }
      const isTooBig = iatWithWindow > now.add('1', 'hours');

      if (isTooBig) {
        log.error('JWT iat is too big ' + payload.iat);
        throw sails.hooks.errorhandler.create('unauthorized', 'expiredToken');
      }

      return resolve(null);
    });
  },

  generateFromValues: function(values) {
    const payload = {
      sub: values.subject,
      type: values.type
    };

    return jwt.sign(payload, values.secret);
  }


};
