/**
 * AuthManager
 *
 * @module      :: Service
 * @description :: Functions for general authentication of
                   AccessKey users(admin, customer)
                   Models most have the following attributes:
                   password, password_recovery_at, name, email.

 */
// const log = sails.hooks.ohmylog.log('services:auth');
const crypto = require('crypto-js/sha3');
const randToken = require('rand-token').generator({
  chars: 'A-Z',
  source: crypto.randomBytes
});
const log = sails.hooks.ohmylog.log('services:authManager');
var getModelName = function(user) {
  return sails.services.modelutils.getModelNameLower(user);
};
const Promise = require('bluebird');

module.exports = {

  hashPassword: function(user) {
    if (!user.changed('password')) {
      return Promise.resolve();
    }
    return new Promise((resolve) => {
      const passwordHash = crypto(user.password).toString();

      user.password = passwordHash;
      resolve();
    });
  },

  validatePassword: function(user, password) {
    const passwordHash = crypto(password).toString();

    if (user.password !== passwordHash) {
      throw sails.hooks.errorhandler.create('unauthorized', 'invalidCredentials');
    }
    return user;
  },

  validateEmail: function(email, model) {
    return model.find({
      where: {
        email: email
      },
      rejectOnEmpty: sails.hooks.errorhandler.rejectOnEmpty('notFound')
    });
  },

  validateLoginData: function(email, password, model) {
    return model.find({
      where: {
        email: email
      },
      rejectOnEmpty: sails.hooks.errorhandler.rejectOnEmpty('unauthorized', 'invalidCredentials')
    })
      .then((user) => {
        return AuthManager.validatePassword(user, password);
      });
  },

  doLogin: function(user) {
    const creationData = {
      subject: user.email,
      secret: randToken.generate(20),
      expires_at: moment().add(sails.config.timing.loginExpiration, 'h')
    };

    creationData[getModelName(user)] = user.id;
    return AccessKey.upsert(creationData, {
      returning: true
    });
  },


  removePasswordRecoveryAccessKey: function(user) {
    const query = {
      where: {
        subject: user.email
      }
    };

    query[getModelName(user)] = user.id;
    return AccessKey.find(query)
      .then((accessKeyForRecovery) => {
        if (accessKeyForRecovery) {
          log.debug('Destroing old accessKey for recvovery');
          return accessKeyForRecovery.destroy({
            paranoid: false
          });
        }

        return Promise.resolve();
      });
  },

  removePasswordRecovery: function(user) {
    return Promise.all([
      user.update({
        password_recovery_at: null
      }),
      AuthManager.removePasswordRecoveryAccessKey(user)
    ]);
  },


  requestPasswordRecovery: function(user) {
    return user.update({
      password_recovery_at: moment()
    })
      .then(() => {
        return AuthManager.removePasswordRecoveryAccessKey(user);
      })
      .then(() => {
        const creationData = {
          subject: user.email,
          secret: randToken.generate(20),
          expires_at: moment().add(sails.config.timing.loginExpiration, 'hours')
        };

        creationData[getModelName(user)] = user.id;

        return AccessKey.create(creationData);
      });
  },

  removeAllAccessKeys: function(user) {
    const query = {
      where: {

      },
      paranoid: false
    };

    query.where[getModelName(user)] = user.id;

    return AccessKey.destroy(query);
  },

  changePassword: function(user, newPassword) {

    return user.update({
      password: newPassword,
      password_recovery_at: null
    })
      .then((userUpdated) => {
        return AuthManager.removeAllAccessKeys(userUpdated);
      });
  },


  sendUserTemplate: function(template, user) {
    const templateName = template;
    const mailOptions = {
      to: user.email,
      template: templateName,
      data: {
        user: user.dataValues,
        urls: sails.config.email.url
      }
    };

    mailOptions.data.recoveryAccessKey = user.recoveryAccessKey;
    return sails.hooks.mailer.send(mailOptions);
  },

  sendEmail: function(templateName, user, extraData) {

    const mailOptions = {
      to: user.email,
      template: templateName,
      attachments: user.attachments || [],
      data: {
        user: user
      }
    };

    if (extraData) {
      mailOptions.data = _.defaults(mailOptions.data, extraData);
    }
    return sails.hooks.mailer.send(mailOptions);
  }


};
