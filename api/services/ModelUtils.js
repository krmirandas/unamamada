/**
 * Utils functions to manage model instances
 */
const retry = require('bluebird-retry');
const Promise = require('bluebird');
const uniq = require('lodash/uniq');


module.exports = {

  addFilesUrlsToFormat: function(format, modelInstance, fileNames) {
    fileNames.forEach((fileName) => {
      const gcsFile = new GCSFile(modelInstance, fileName, null);

      format[fileName] = gcsFile.getFullUrl();
    });
  },

  addFilesUrlList: function(format, modelInsctance, fileName) {
    fileName.forEach((fileName) => {
      const gcsFile = new GCSFile(modelInstance, fileName, null);

      format[fileName] = gcsFile.getFullUrl();
    });
  },

  addFilesUrlsToFormatArray: function(format, modelInstance, arrays) {
    arrays.forEach((array) => {
      const length = modelInstance.length;

      for (i = 1; i <= length; i++) {
        const fileName = array + '-' + i;
        const gcsFile = new GCSFile(modelInstance, fileName, null);

        format[fileName] = gcsFile.getFullUrl();
      }
    });
  },
  getModelName: function(modelInstance, plural = false) {
    const modelName = modelInstance._modelOptions.name[plural ? 'plural' : 'singular'];

    return modelName;
  },
  getModelNameLower: function(modelInstance, plural) {
    return sails.services.modelutils.getModelName(modelInstance, plural).toLowerCase();
  },
  getModelNameFirstLower: function(modelInstance, plural) {
    const name = sails.services.modelutils.getModelName(modelInstance, plural);

    // For example: FAQ
    if (name == name.toUpperCase()) {
      return name.toLowerCase();
    }

    return name[0].toLowerCase() + name.slice(1);
  },

  updateAttrsPatch: function(modelInstance, updateFields) {
    const modelName = sails.services.modelutils.getModelNameLower(modelInstance);
    const modelAttrs = sails.models[modelName].basicAttributes();

    return modelInstance.update(_.pick(updateFields, modelAttrs));
  },

  validateCondtionalModel: function(id, model) {
    return model.findById(id) !== null;
  },

  validateModel: function(id, model) {
    return model.findById(id, {
      rejectOnEmpty: sails.hooks.errorhandler.rejectOnEmptyModel(model)
    });
  }
};
