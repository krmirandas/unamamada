/**
 * Manages errors to be used with res.negotiate
 */

class ErrorHandler {
  constructor(statusName, message, meta, errors = []) {
    this.name = 'ErrorHandler';
    this.statusName = statusName;
    this.message = message;
    this.meta = meta;
    this.errors = errors;
  }

  status() {
    const status = sails.config.errorhandler[this.statusName];

    if (!status) {
      sails.hooks.ohmylog.log('errorhandler:idex').error('Missing code for ' + this.statusName + ' sending 500');
      return 500;
    }

    return status;
  }

  respond(t) {
    let errorBody;
    const codesMap = sails.config.errorhandler.codes;

    if (typeof this.message == 'string') {
      const codeError = codesMap[this.message];

      if (!codeError) {
        sails.hooks.ohmylog.log('errorhandler:idex').warn('Error code ' + this.message + ' not found, using status');
        this.message = this.statusName;
      }

      errorBody = {
        message: t(this.message, this.meta),
        code: codeError
      };
    } else {
      //Multiple errors, sequelize or request handler
      //422 400
      errorBody = {
        message: t(this.statusName),
        code: codesMap[this.statusName],
        errors: []
      };
      if (Array.isArray(this.message)) {
        this.message.forEach((e) => {
          const m = typeof e.message == 'string' ? e.message : e.message.message;
          const mt = e.message.meta || {};

          errorBody.errors.push({
            fields: e.fields,
            message: t(m, mt)
          });
        });
      } else {

        for (const key in this.message) {
          const localErrors = [];
          const fieldError = this.message[key];

          if (typeof fieldError == 'string') {
            localErrors.push(
              {
                field: key,
                message: t(fieldError, this.meta),
                code: codesMap[fieldError]
              }
            );
          } else {
            // fieldError = [{message: 'blabla', meta: {}}]
            fieldError.forEach((e) => {
              localErrors.push(
                {
                  field: key,
                  message: t(e.message, e.meta),
                  code: codesMap[e.message]
                }
              );
            });
          }

          errorBody.errors = errorBody.errors.concat(localErrors);
        }
      }
      // message = {name: [a, b], secret: 'blabla'}
    }

    return errorBody;
  }
}

module.exports = function() {
  return {
    defaults: {
      errorhandler: {
        invalid: 422,
        locked: 423,
        conflict: 409,
        badRequest: 400,
        forbidden: 403,
        unauthorized: 401,
        unavailableForLegalReasons: 451,
        failedDependency: 424,
        preconditionFailed: 412,
        unsupportedMediaType: 415,
        notImplemented: 501,
        gone: 410,
        tooManyRequests: 429,
        notFound: 404,
        serverError: 500
      }
    },

    create: function(statusName, message, meta) {
      return new ErrorHandler(statusName, message, meta);
    },

    rejectOnEmpty: function(statusName = 'notFound', message, meta = {}) {
      messsage = message || 'notFound';
      return new ErrorHandler(statusName, messsage, meta);
    },

    rejectOnEmptyModel: function(modelName) {
      return sails.hooks.errorhandler.rejectOnEmpty('notFound', 'notFound {{model}}', {model: modelName});
    },

    fromSequelize(err) {
      console.log(err);
      if (err instanceof Sequelize.EmptyResultError) {
        return new ErrorHandler('notFound', 'notFound {{model}}', {model: err.model});
      } else if (err instanceof Sequelize.DatabaseError) {
        sails.hooks.ohmylog.log('errorhandler:idex').error('Database error: ', err.message);
        return new ErrorHandler('serverError', 'databaseError');
      } else if (err instanceof Sequelize.UniqueConstraintError) {
        const key = err.errors[0].path;

        return new ErrorHandler('conflict', key + 'Unique');
      } else if (err instanceof Sequelize.ValidationError) {
        const detailedErrors = {};

        err.errors.forEach((e) => {
          detailedErrors[e.path] = detailedErrors[e.path] || [];
          if (e.type == 'notNull Violation') {
            detailedErrors[e.path].push({
              message: 'notNull',
              meta: {path: e.path}
            });
          } else {
            const components = e.message.split('->');
            const error = {};

            if (components.length == 1) {
              error.message = components[0];
              error.meta = {path: e.path};
            } else {
              error.message = components[0] + ' {{value}}';
              error.meta = {path: e.path, value: components[1]};
            }
            detailedErrors[e.path].push(error);
          }
        });
        return new ErrorHandler('invalid', detailedErrors);
      }
      throw new TypeError(err.name + ' unknown');
    },

    validations: {
      min: (v) => {
        return {
          args: [v],
          msg: 'min->' + v
        };
      },

      max: (v) => {
        return {
          args: [v],
          msg: 'max->' + v
        };
      },

      len: (a, b = '') => {
        const args = b == '' ? a : [a, b];
        let type = b == '' ? 'lenExact' : 'lenHave';
        let msg = b == '' ? String(a) : a + '-' + b;

        if (a == 0) {
          type = 'lenMost';
          msg = b;
        }
        return {
          args: args,
          msg: type + '->' + msg
        };
      },

      email: {
        args: true,
        msg: 'invalidFormat'
      },

      in: (args) => {
        return {
          args: [args],
          msg: 'in->' + args.join(', ')
        };
      }
    }
  };
};
