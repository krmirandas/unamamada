const nodemailer = require('nodemailer');
const htmlToText = require('nodemailer-html-to-text').htmlToText;
const omit = require('lodash/omit');
var sendMail = function(options) {
  let transporter;
  const render = Promise.promisify(sails.hooks.views.render);

  options.attachments = options.attachments || [];
  const log = sails.hooks.ohmylog.log('hooks:mailer');

  return new Promise((resolve) => {
    if (!options.to) {
      throw new Error('Cant send mail without to');
    }

    log.debug('About to create transport with Sails configuration');
    return resolve(nodemailer.createTransport(sails.config.email.transport));
  })
    .then((res) => {
      transporter = res;
      transporter.use(htmlToText);
      return Promise.promisify(transporter.verify);
    })
    .then(() => {
      if (options.html) {
        return options.html;
      }

      const data = options.data;
      const template = sails.config.email.templates[options.template];


      if (!template) {
        log.error('Not found template ' + options.template);
        throw sails.hooks.errorhandler.create('serverError', 'serverError');
      }


      options = omit(options, ['data']);
      if (template.view) {
        return render('email/' + template.view, data);
      }
      if (template.html) {
        return template.html(data);
      }
      if (template.text) {
        return data.toString();
      }

      log.warn('Sending empty email.');
      return '';
    })
    .then((html) => {
      const subject = options.subject || sails.config.email.templates[options.template].subject;

      options.html = html;
      options.from = options.from || sails.config.email.from;
      options.subject = typeof subject == 'function' ? subject(options.options) : subject;
      sails.hooks.ohmylog.info('Sending email to: ' + options.to + ' for ' + options.template);
      return transporter.sendMail(omit(options, ['options', 'template']));
    })
    .then((info) => {
      log.info('Email ' + info.messageId + ' sent, got ' + info.response); // messageId
    })
    .catch((err) => {
      log.error('Cant send email due to error', err.message);
      throw sails.hooks.errorhandler.create('failedDependency', 'emailError');
    });
};

module.exports = function(sails) {
  return {
    defaults: {
      email: {
        testMode: false
      }
    },

    configure: function() {
      if (sails.config.email.testMode) {
        sails.config.email.transport = {
          jsonTransport: true
        };
      }
    },

    send: sendMail
  };
};
