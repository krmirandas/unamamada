/**
*Usage:
*At the begining of the file create log variable
*var log = sails.hooks.ohmylog.log('folder:fileName');
*In any part of the file use 'info', 'warn', 'error' or 'debug':
* log.info("Mesage");
*/

var logger = require('./lib/logger');
var factoryDecorator = require('./lib/factory');

module.exports = function(sails) {
  return {
    defaults: {
      ohmylog: {
        // global config
        colors: {
          debug: 'blue',
          info: 'green',
          warn: 'yellow',
          error: 'red',
          verbose: 'gray',
          silly: 'magenta'
        },
        levels: {
          error: 0,
          warn: 1,
          info: 2,
          debug: 3,
          verbose: 4,
          silly: 5
        },
        // logging style
        length: 24,
        defaultLabel: 'ohmylog',
        colorize: true,
        inspect: false,
        // console defaults
        console: {
          level: process.env.CLEVEL || 'debug'
        },
        // file defaults
        file: {
          filename: 'ohmylog.log',
          level: process.env.FLEVEL || 'warn',
          maxDays: 30
        },
        // where exceptions are written
        exceptionFilename: 'exceptions.log',
        // if file should rotate
        rotate: false,
        defaultPattern: 'dd-MM-yy_',
        // no more loggers
        extras: []
      }
    },

    configure: function() {
      if (sails.config.ohmylog.rotate === true) {
        sails.config.ohmylog.rotate = sails.config.ohmylog.defaultPattern;
      }
    },

    initialize: function(cb) {
      var factory = factoryDecorator();

      sails.hooks.ohmylog.default = factory.default;

      const defaultLogger = logger(sails, factory.default)(sails.config.ohmylog.defaultLabel);

      sails.hooks.ohmylog.info = defaultLogger.info;
      sails.hooks.ohmylog.error = defaultLogger.error;
      sails.hooks.ohmylog.debug = defaultLogger.debug;
      sails.hooks.ohmylog.warn = defaultLogger.warn;
      sails.hooks.ohmylog.verbose = defaultLogger.verbose;
      sails.hooks.ohmylog.silly = defaultLogger.silly;

      if (sails.config.ohmylog.extras.length > 0) {
        sails.config.ohmylog.extras.forEach(function(x) {
          const extraLogger = factory.loadLogger(x);
          const loggerWrap = logger(sails, extraLogger)(x.name);

          sails.hooks.ohmylog[x.name] = loggerWrap;
        });
      }
      return cb();
    },

    log: logger(sails)

  };
};
