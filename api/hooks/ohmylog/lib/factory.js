'use strict';

const winston = require('winston');
const moment = require('moment');

require('winston-daily-rotate-file');

const getTransport = function(hasType, type) {
  let transport = null;

  if (hasType && winston.loggers.options.transports[0].name === type) {
    transport = winston.loggers.options.transports[0];
  } else if (hasType && winston.loggers.options.transports.length > 1 && winston.loggers.options.transports[1].name === type) {
    transport = winston.loggers.options.transports[1];
  }
  return transport;
};


module.exports = () => {
  winston.loggers.options.transports = [
    new (sails.config.ohmylog.rotate !== false ? winston.transports.DailyRotateFile : winston.transports.File)({
      name: 'file_global',
      filename: sails.config.ohmylog.file.filename,
      datePattern: sails.config.ohmylog.rotate,
      prepend: true,
      maxDays: sails.config.ohmylog.file.maxDays,
      level: sails.config.ohmylog.file.level,
      json: false,
      prettyPrint: true,
      timestamp: function() {
        return moment().format('MM/D/YY, HH:mm:ss');
      }
    }),
    new winston.transports.Console({
      level: sails.config.ohmylog.console.level,
      colorize: sails.config.ohmylog.colorize,
      prettyPrint: true,
      showLevel: false
    })
  ];

  return {
    default: new winston.Logger({
      levels: sails.config.ohmylog.levels,
      colors: sails.config.ohmylog.colors,
      transports: winston.loggers.options.transports,
      exitOnError: false,
      exceptionHandlers: [
        new winston.transports.File({
          filename: sails.config.ohmylog.exceptionFilename,
          json: false,
          prettyPrint: true,
          timestamp: function() {
            return '\n' + moment().format('MM/D/YY, HH:mm:ss') + ' __________________ CRASH __________________\n';
          }
        })
      ]
    }),

    loadLogger: function(config) {
      const transports = [];

      if (config.console !== null && config.console != false) {
        const g = config.console !== true ? config.console : {};
        const consoleConfig = _.defaults(g, {
          name: config.name + '_console',
          level: sails.config.ohmylog.console.level,
          colorize: sails.config.ohmylog.colorize,
          prettyPrint: true,
          showLevel: false,
          fallback: false
        });

        if (!config.fallback) {
          transports.push(new winston.transports.Console(consoleConfig));
        } else {
          const levelGlobal = sails.config.ohmylog.levels[sails.config.ohmylog.console.level];
          const levelExtra = sails.config.ohmylog.levels[consoleConfig.level];

          if (levelExtra > levelGlobal) { // only add custom console
            transports.push(new winston.transports.Console(consoleConfig));
          } else {
            const hasConsole = winston.loggers.options.transports.length > 0;
            const consoleT = getTransport(hasConsole, 'console');
            const consoleFT = consoleT || new winston.transports.Console(consoleConfig);

            transports.push(consoleFT);
          }
        }
      }

      if (config.file !== null && config.fle != false) {
        const g = config.file !== true ? config.file : {};
        const fileConfig = _.defaults(g, {
          name: config.name + '_file',
          filename: config.name + '.log',
          level: sails.config.ohmylog.file.level,
          json: false,
          prettyPrint: true,
          timestamp: function() {
            return moment().format('MM/D/YY, HH:mm:ss');
          },
          rotate: false,
          fallback: false
        });

        if (fileConfig.rotate === true) {
          fileConfig.datePattern = typeof sails.config.ohmylog.rotate === 'string'
            ? sails.config.ohmylog.rotate : sails.config.ohmylog.defaultPattern; // load global or default
          fileConfig.prepend = true;
        } else if (typeof fileConfig.rotate === 'string') {
          fileConfig.datePattern = fileConfig.rotate;
          fileConfig.prepend = true;
        }

        transports.push(new (fileConfig.datePattern !== null ? winston.transports.DailyRotateFile : winston.transports.File)(fileConfig));
        if (config.fallback) {
          const hasFile = winston.loggers.options.transports.length > 0;
          const fileT = getTransport(hasFile, 'file_global');

          if (fileT) {
            transports.push(fileT);
          }
        }
      }

      return new winston.Logger({
        levels: sails.config.ohmylog.levels,
        colors: sails.config.ohmylog.colors,
        transports: transports
      });
    }
  };
};
