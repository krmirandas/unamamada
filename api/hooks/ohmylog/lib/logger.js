const chalk = require('chalk');
var prettyLog = function(type, label, fill, argument) {
  const strColor = sails.config.ohmylog.colors[type];
  const colorizeStart = argument !== '' ? chalk[strColor](label + fill) : chalk[strColor](label);
  const message = typeof argument === 'object' ? JSON.stringify(argument, null, 2) : colorizeStart + ' ' + argument;

  return message;
};

module.exports = function(sails, customLogger) {
  return function(logOption) {
    const maxLogOptionLength = sails.config.ohmylog.length;
    let fill = '';

    for (let i = logOption.length; i < maxLogOptionLength; i++) {
      fill += '·';
    }

    return {
      info: function() {
        let regex = { test: () => true };

        if (process.env.IONLY) {
          regex = new RegExp(process.env.IONLY, 'g');
        }

        if (!regex.test(logOption)) {
          return;
        }

        const logger = customLogger || sails.hooks.ohmylog.default;

        _.map(arguments, (argument) => {
          logger.info(prettyLog('info', logOption, fill, argument));
        });
      },
      warn: function() {
        let regex = { test: () => true };

        if (process.env.WONLY) {
          regex = new RegExp(process.env.WONLY, 'g');
        }

        if (!regex.test(logOption)) {
          return;
        }

        const logger = customLogger || sails.hooks.ohmylog.default;

        _.map(arguments, (argument) => {
          logger.warn(prettyLog('warn', logOption, fill, argument));
        });
      },
      error: function() {
        let regex = { test: () => true };

        if (process.env.EONLY) {
          regex = new RegExp(process.env.EONLY, 'g');
        }

        if (!regex.test(logOption)) {
          return;
        }

        const logger = customLogger || sails.hooks.ohmylog.default;

        _.map(arguments, (argument) => {
          logger.error(prettyLog('error', logOption, fill, argument));
        });
      },
      debug: function() {
        let regex = { test: () => true };

        if (process.env.DONLY) {
          regex = new RegExp(process.env.DONLY, 'g');
        }

        if (!regex.test(logOption)) {
          return;
        }

        const logger = customLogger || sails.hooks.ohmylog.default;

        _.map(arguments, (argument) => {
          logger.debug(prettyLog('debug', logOption, fill, argument));
        });
      },
      verbose: function() {
        let regex = { test: () => true };

        if (process.env.VONLY) {
          regex = new RegExp(process.env.VONLY, 'g');
        }

        if (!regex.test(logOption)) {
          return;
        }

        const logger = customLogger || sails.hooks.ohmylog.default;

        _.map(arguments, (argument) => {
          logger.verbose(prettyLog('verbose', logOption, fill, argument));
        });
      },
      silly: function() {
        let regex = { test: () => true };

        if (process.env.SONLY) {
          regex = new RegExp(process.env.SONLY, 'g');
        }

        if (!regex.test(logOption)) {
          return;
        }

        const logger = customLogger || sails.hooks.ohmylog.default;

        _.map(arguments, (argument) => {
          logger.silly(prettyLog('silly', logOption, fill, argument));
        });
      }
    };
  };
};
