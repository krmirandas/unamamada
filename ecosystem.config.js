module.exports = {
  apps: [
    {
      name: 'unearth_development',
      script: './app.js',
      env: {
        NODE_ENV: 'development'
        // otras variables de entorno
      }
    },
    {
      name: 'unearth_staging',
      script: './app.js',
      env: {
        NODE_ENV: 'staging'
        // otras variables de entorno
      }
    }
  ]
};
